FROM ruby:2.3
MAINTAINER james.stoup@yazdaapp.com

RUN apt-get update && apt-get install -y \
    build-essential \
    nodejs \
    imagemagick \
    libpq-dev \
    postgresql-client-9.4

RUN mkdir -p /volunteer
WORKDIR /volunteer

COPY Gemfile Gemfile.lock ./
RUN gem update --system
RUN gem install bundler && bundle install --jobs 20 --retry 5

COPY . ./
