class ChartOptions < Dry::Struct
  constructor_type :schema

  attribute :tags, Types::Coercible::String.optional
  attribute :url, Types::Url.default('volunteer')
  attribute :context, Types::Coercible::String.optional
  attribute :start_time, Types::Date.optional
  attribute :end_time, Types::Date.optional
  attribute :top, Types::Int.optional
  attribute :period, Types::Period.optional
  attribute :op, Types::Operation.optional
  attribute :primary_color, Types::Color.optional
  attribute :secondary_color, Types::Color.optional
  attribute :width, Types::Int.default(12)
end
