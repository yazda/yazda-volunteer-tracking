module Types
  include Dry::Types.module

  Color     = String.constrained(format: /\A([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})\z/)
  Period    = String.constrained(format: /\Aday|week|month|quarter|year|day_of_week|day_of_month|month_of_year\z/)
  Operation = String.constrained(format: /\Acount|hours\z/)
  Url = String.constrained(format: /\Avolunteer|tags|adventures\z/)
end
