class TextOptions < Dry::Struct
  constructor_type :schema

  attribute :primary_color, Types::Color.optional
  attribute :secondary_color, Types::Color.optional
  attribute :width, Types::Int.default(12)
end
