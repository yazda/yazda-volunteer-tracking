// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).on('turbolinks:load', function() {
  $('#user_skill_list').selectize({
    plugins: ['remove_button'],
    valueField:  'name',
    labelField:  'name',
    searchField: 'name',
    delimiter:   ',',
    persist:     false,
    create:      true,
    load:        function(query, callback) {
      if(!query.length) {
        return callback();
      }
      $.ajax({
        url:     '/tags?context=skills&q=' + encodeURIComponent(query),
        type:    'GET',
        error:   function() {
          callback();
        },
        success: function(res) {
          callback(res);
        }
      });
    },
    render:      {
      option: function(item, escape) {
        return '<p>' + escape(item.name) + '</p>';
      }
    }
  });
});
