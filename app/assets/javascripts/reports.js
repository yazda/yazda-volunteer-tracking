// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).on('turbolinks:load', function() {

  $(function() {
    cloneDraggable = function(item) {
      var item_clone = item.clone();
      item.data("clone", item_clone);
      var position = item.position();
      item_clone
        .css({
          left: "0",
          bottom: "0",
          visibility: "hidden"
        })
        .attr("data-pos", i + 1);

      // TODO Mark -> this element doesn't exist in the DOM, how does it work?$("#cloned-slides").append(item_clone);
      $("#cloned-slides").append(item_clone);
    };

    $(".canvas-item").each(function(i) {
      cloneDraggable($(this));
    });

    refreshDraggable = function() {
      $(".drag-container").sortable({
        handle: ".handle",
        revert: true,
        forcePlaceholderSize: true,
        placeholder: "drag-drop-placeholder",
        cursor: "move",

        start: function(e, ui) {
          ui.helper.addClass("exclude-me");
          $(".all-slides .slide:not(.exclude-me)")
            .css("visibility", "hidden");
          // TODO Mark -> this was failing  for some reason so I've
          // commented it out.
          // ui.helper.data("clone").hide();
          $(".cloned-slides .slide").css("visibility", "visible");
        },

        stop: function(e, ui) {
          $(".all-slides .slide.exclude-me").each(function() {
            var item = $(this);
            var clone = item.data("clone");
            var position = item.position();

            clone.css("left", "0");
            clone.css("bottom", "0");
            clone.show();

            item.removeClass("exclude-me");
          });

          $(".all-slides .slide").each(function() {
            var item = $(this);
            var clone = item.data("clone");

            clone.attr("data-pos", item.index());
          });

          $(".all-slides .slide").css("visibility", "visible");
          $(".cloned-slides .slide").css("visibility", "hidden");

          $.ajax({
            url: ui.item.data('report-url'),
            data: {
              report_item: {
                position: ui.item.index() + 1
              }
            },
            type: 'PUT'
          });
        },

        change: function(e, ui) {
          $(".all-slides .slide:not(.exclude-me)").each(function() {
            var item = $(this);
            var clone = item.data("clone");
            clone.stop(true, false);
            var position = item.position();
            clone.animate({
              left: "0",
              bottom: "0"
            }, 500);
          });
        }

      });
    };

    refreshDraggable();
    jscolor.installByClassName('jscolor');
  });
});

// Auto expand applied globally on all textareass
$(document).on('turbolinks:load', function() {


});
