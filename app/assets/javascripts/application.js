// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require d3
//= require c3
//= require clipboard
//= require js.cookie
//= require jstz
//= require browser_timezone_rails/set_time_zone
//= require foundation
//= require jquery.purr
//= require best_in_place
//= require datedropper
//= require timedropper
//= require selectize
//= require cocoon
//= require social-share-button
//= require turbolinks
//= require_tree .

BestInPlaceEditor.defaults.purrErrorContainer = '<div id="error"' +
    ' class="error" data-closable><span class="error-text"></span> <button' +
    ' class="close-button" aria-label="Dismiss alert" type="button"' +
    ' data-close> <span aria-hidden="true"><i class="ion-close"></i></span>' +
    ' </button> </div>';

BestInPlaceEditor.defaults.purrNoticeContainer = '<div id="notice"' +
    ' class="volunteer-notice" data-closable><span' +
    ' class="notice-text"></span>' +
    ' <button class="close-button" aria-label="Dismiss alert" type="button"' +
    ' data-close> <span aria-hidden="true"><i class="ion-close"></i></span>' +
    ' </button> </div>';

$(document).on('best_in_place:error', function(event, request, error) {
  'use strict';
  // Display all error messages from server side validation
  jQuery.each(jQuery.parseJSON(request.responseText), function(index, value) {
    if(typeof value === "object") {
      value = index + " " + value.toString();
    }
    var container = $(BestInPlaceEditor.defaults.purrErrorContainer);
    container.find('.error-text').html(value);
    container.purr({
      isSticky: true
    });
    setTimeout(function() {
      $('#purr-container').remove();
    }, 4000);
  });
});

$(document).on('turbolinks:load', function() {
  $(document).foundation();

  $('.form-date-picker').dateDropper();
  $('.form-time-picker').timeDropper({
    format:     'h:mm A',
    mousewheel: true,
    meridians:  true,
    primaryColor: '#299855',
    textColor: '#299855',
    borderColor: '#8a8a8a',
    backgroundColor: '#303030'
  });

  // Capitalize all a link title attribute text
  Array.from(document.querySelectorAll('[title]')).forEach(function(el) {
    return el.title = el.title.replace(/\b([a-z])/g, function(m) {
      return m.toUpperCase();
    });
  });

  // Fade Out Errors/Alerts after period of time
  $('#volunteer-notice').delay(3000).fadeOut(500);
  $('#volunteer-error').delay(3000).fadeOut(500);

  // Copy to clipboard
  var clipboard = new Clipboard('.copy-button');

  // Textarea Autogrow
  $('textarea').autogrow();
});
