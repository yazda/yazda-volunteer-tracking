/*
 * YazdaChart.js
 * Create beautiful charts with one line of JavaScript
 * https://github.com/ankane/YazdaChart.js
 * v2.2.3
 * MIT License
 */

/*jslint browser: true, indent: 2, plusplus: true, vars: true */

(function(window) {
  'use strict';

  var config = window.YazdaChart || {};
  var YazdaChart, ISO8601_PATTERN, DECIMAL_SEPARATOR, adapters = [];
  var DATE_PATTERN = /^(\d\d\d\d)(\-)?(\d\d)(\-)?(\d\d)$/i;
  var C3ChartsAdapter;
  var pendingRequests = [], runningRequests = 0, maxRequests = 4;

  // helpers
  function ColorLuminance(hex, lum) {

    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if(hex.length < 6) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    lum = lum || 0;

    // convert to decimal and change luminosity
    var rgb = "#", c, i;
    for(i = 0; i < 3; i++) {
      c = parseInt(hex.substr(i * 2, 2), 16);
      c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
      rgb += ("00" + c).substr(c.length);
    }

    return rgb;
  }

  function isArray(variable) {
    return Object.prototype.toString.call(variable) === "[object Array]";
  }

  function isFunction(variable) {
    return variable instanceof Function;
  }

  function isPlainObject(variable) {
    return !isFunction(variable) && variable instanceof Object;
  }

  // https://github.com/madrobby/zepto/blob/master/src/zepto.js
  function extend(target, source) {
    var key;
    for(key in source) {
      if(isPlainObject(source[key]) || isArray(source[key])) {
        if(isPlainObject(source[key]) && !isPlainObject(target[key])) {
          target[key] = {};
        }
        if(isArray(source[key]) && !isArray(target[key])) {
          target[key] = [];
        }
        extend(target[key], source[key]);
      } else if(source[key] !== undefined) {
        target[key] = source[key];
      }
    }
  }

  function merge(obj1, obj2) {
    var target = {};
    extend(target, obj1);
    extend(target, obj2);
    return target;
  }

  // https://github.com/Do/iso8601.js
  ISO8601_PATTERN = /(\d\d\d\d)(\-)?(\d\d)(\-)?(\d\d)(T)?(\d\d)(:)?(\d\d)?(:)?(\d\d)?([\.,]\d+)?($|Z|([\+\-])(\d\d)(:)?(\d\d)?)/i;
  DECIMAL_SEPARATOR = String(1.5).charAt(1);

  function parseISO8601(input) {
    var day, hour, matches, milliseconds, minutes, month, offset, result, seconds, type, year;
    type = Object.prototype.toString.call(input);
    if(type === "[object Date]") {
      return input;
    }
    if(type !== "[object String]") {
      return;
    }
    matches = input.match(ISO8601_PATTERN);
    if(matches) {
      year = parseInt(matches[1], 10);
      month = parseInt(matches[3], 10) - 1;
      day = parseInt(matches[5], 10);
      hour = parseInt(matches[7], 10);
      minutes = matches[9] ? parseInt(matches[9], 10) : 0;
      seconds = matches[11] ? parseInt(matches[11], 10) : 0;
      milliseconds = matches[12] ? parseFloat(DECIMAL_SEPARATOR + matches[12].slice(1)) * 1000 : 0;
      result = Date.UTC(year, month, day, hour, minutes, seconds, milliseconds);
      if(matches[13] && matches[14]) {
        offset = matches[15] * 60;
        if(matches[17]) {
          offset += parseInt(matches[17], 10);
        }
        offset *= matches[14] === "-" ? -1 : 1;
        result -= offset * 60 * 1000;
      }
      return new Date(result);
    }
  }

  // end iso8601.js

  function setText(element, text) {
    if(document.body.innerText) {
      element.innerText = text;
    } else {
      element.textContent = text;
    }
  }

  function chartError(element, message) {
    setText(element, "Error Loading Chart: " + message);
    element.style.color = "#ff0000";
  }

  function pushRequest(element, url, success) {
    pendingRequests.push([element, url, success]);
    runNext();
  }

  function runNext() {
    if(runningRequests < maxRequests) {
      var request = pendingRequests.shift();
      if(request) {
        runningRequests++;
        getJSON(request[0], request[1], request[2]);
        runNext();
      }
    }
  }

  function requestComplete() {
    runningRequests--;
    runNext();
  }

  function getJSON(element, url, success) {
    ajaxCall(url, success, function(jqXHR, textStatus, errorThrown) {
      var message = (typeof errorThrown === "string") ? errorThrown : errorThrown.message;
      chartError(element, message);
    });
  }

  function ajaxCall(url, success, error) {
    var $ = window.jQuery || window.Zepto || window.$;

    if($) {
      $.ajax({
        dataType: "json",
        url:      url,
        success:  success,
        error:    error,
        complete: requestComplete
      });
    } else {
      var xhr = new XMLHttpRequest();
      xhr.open("GET", url, true);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.onload = function() {
        requestComplete();
        if(xhr.status === 200) {
          success(JSON.parse(xhr.responseText), xhr.statusText, xhr);
        } else {
          error(xhr, "error", xhr.statusText);
        }
      };
      xhr.send();
    }
  }

  function errorCatcher(chart, callback) {
    try {
      callback(chart);
    } catch(err) {
      chartError(chart.element, err.message);
      throw err;
    }
  }

  function fetchDataSource(chart, callback, dataSource) {
    if(typeof dataSource === "string") {
      pushRequest(chart.element, dataSource, function(data, textStatus, jqXHR) {
        chart.rawData = data;
        errorCatcher(chart, callback);
      });
    } else {
      chart.rawData = dataSource;
      errorCatcher(chart, callback);
    }
  }

  // type conversions

  function toStr(n) {
    return "" + n;
  }

  function toFloat(n) {
    return parseFloat(n);
  }

  function toDate(n) {
    var matches, year, month, day;
    if(typeof n !== "object") {
      if(typeof n === "number") {
        n = new Date(n * 1000); // ms
      } else if((matches = n.match(DATE_PATTERN))) {
        year = parseInt(matches[1], 10);
        month = parseInt(matches[3], 10) - 1;
        day = parseInt(matches[5], 10);
        return new Date(year, month, day);
      } else { // str
        // try our best to get the str into iso8601
        // TODO be smarter about this
        var str = n.replace(/ /, "T").replace(" ", "").replace("UTC", "Z");
        n = parseISO8601(str) || new Date(n);
      }
    }
    return n;
  }

  function toArr(n) {
    if(!isArray(n)) {
      var arr = [], i;
      for(i in n) {
        if(n.hasOwnProperty(i)) {
          arr.push([i, n[i]]);
        }
      }
      n = arr;
    }
    return n;
  }

  function sortByTime(a, b) {
    return a[0].getTime() - b[0].getTime();
  }

  function sortByNumberSeries(a, b) {
    return a[0] - b[0];
  }

  function sortByNumber(a, b) {
    return a - b;
  }

  function loadAdapters() {
    if(!C3ChartsAdapter && "d3" in window && "c3" in window) {
      C3ChartsAdapter = new function() {
        var c3 = window.c3;

        this.name = "c3";

        //TODO Mark set C3.js base options here (for every chart)
        var baseOptions = {
          tooltip: {
            grouped: false,
            title: false
          },
          line: {
          	connectNull: true
          },
          point: {
            r: 10,
            focus: {
              expand: {
                r: 20
              }
            }
          }
        };

        // TODO set C3.js default options (for most charts) here
        //Set Default Options Here
        var defaultOptions = {};

        // http://there4.io/2012/05/02/google-chart-color-list/
        var drawChart = function(chart, type, data, options) {
          var i = 1;
          var primaryColor = options.primaryColor || '#299855';
          var complementaryColor = options.secondaryColor || '#F26530';
          var colors = {};

          data.type = type;

          if(data.x) {
            options.axis = {
              x: {
                type: 'timeseries',
                tick: {
                  format: '%y-%m-%d',
                  centered: true,
                  count: 5
                },
                show: false
              }
            };
          }

          if(chart.chart) {
            chart.chart.destroy();
          } else {
            chart.element.innerHTML = "<div></div>";
          }

          var ctx = chart.element.getElementsByTagName("div")[0];
          chart.chart = c3.generate({
            bindto:  ctx,
            data:    $.extend({}, data, {
              color: function(color, d) {
                var lightness = i * Object.keys(colors).length / 20;
                var c = (lightness * 20) % 2 === 0 ? primaryColor : complementaryColor;

                // d will be 'id' when called for legends
                if(typeof d === 'object') {
                  if(colors[d.id]) {
                    return colors[d.id];
                  } else {
                    colors[d.id] = ColorLuminance(c, lightness);
                  }

                  return colors[d.id];
                } else {
                  if(colors[d]) {
                    return colors[d];
                  } else {
                    colors[d] = ColorLuminance(c, lightness);
                    i = -i;
                  }

                  return colors[d];
                }
                //return d.id && d.id === 'data3' ? d3.rgb(color).darker(d.value / 150) : color;
                // }
              }
            }),
            color:   options.color,
            axis:    options.axis,
            grid:    options.grid,
            regions: options.regions,
            legend:  options.legend,
            tooltip: options.tooltip,
            line:    options.line,
            area:    options.area,
            bar:     options.bar,
            pie:     options.pie,
            donut:   options.donut
          });
        };

        this.renderLineChart = function(chart, chartType) {
          var type = chartType ? chartType : 'line';
          var defaultLineOptions = {};
          var options = $.extend({}, baseOptions, defaultOptions, defaultLineOptions, chart.options);

          drawChart(chart, type, chart.data, options);
        };

        this.renderDonutChart = function(chart) {
          var defaultDonutOptions = {
            borderColor: "#000000",
            strokeWidth: 4
          };
          var options = $.extend({}, baseOptions, defaultOptions, defaultDonutOptions, chart.options);

          drawChart(chart, "donut", chart.data, options);
        };

        this.renderPieChart = function(chart) {
          var defaultPieChartOptions = {};
          var options = $.extend({}, baseOptions, defaultOptions, defaultPieChartOptions, chart.options);

          drawChart(chart, "pie", chart.data, options);
        };

        var self = this;

        this.renderAreaChart = function(chart) {
          self.renderLineChart(chart, "area");
        };

        this.renderBarChart = function(chart) {
          var defaultBarChartOptions = {
            bar: {
              width: {
                ratio:  1,
                stroke: 4
              }
            }
          };

          var options = $.extend({}, baseOptions, defaultOptions, defaultBarChartOptions, chart.options);
          drawChart(chart, 'bar', chart.data, options);
        };

        this.renderScatterChart = function(chart) {
          var defaultScatterChartOptions = {
            // TODO Can't set options for individual chart types
            axis: {
              x: {
                show: false
              }
            },
            point: {
              r: 10,
              focus: {
                r: 20
              }
            }
          };

          var options = $.extend({}, baseOptions, defaultOptions, defaultScatterChartOptions, chart.options);

          drawChart(chart, 'scatter', chart.data, options);
        };
      };

      adapters.unshift(C3ChartsAdapter);
    }
  }

  function renderChart(chartType, chart) {
    callAdapter(chartType, chart);
  }

  // TODO remove chartType if cross-browser way
  // to get the name of the chart class
  function callAdapter(chartType, chart) {
    var i, adapter, fnName, adapterName;
    fnName = "render" + chartType;
    adapterName = chart.options.adapter;

    loadAdapters();

    for(i = 0; i < adapters.length; i++) {
      adapter = adapters[i];
      if((!adapterName || adapterName === adapter.name) && isFunction(adapter[fnName])) {
        chart.adapter = adapter.name;
        return adapter[fnName](chart);
      }
    }
    throw new Error("No adapter found");
  }

  // process data

  var toFormattedKey = function(key, keyType) {
    if(keyType === "number") {
      key = toFloat(key);
    } else if(keyType === "datetime") {
      key = toDate(key);
    } else {
      key = toStr(key);
    }
    return key;
  };

  var formatSeriesData = function(data, keyType) {
    var r = [], key, j;
    for(j = 0; j < data.length; j++) {
      if(keyType === "bubble") {
        r.push([toFloat(data[j][0]), toFloat(data[j][1]), toFloat(data[j][2])]);
      } else {
        key = toFormattedKey(data[j][0], keyType);
        r.push([key, toFloat(data[j][1])]);
      }
    }
    if(keyType === "datetime") {
      r.sort(sortByTime);
    } else if(keyType === "number") {
      r.sort(sortByNumberSeries);
    }
    return r;
  };

  function isMinute(d) {
    return d.getMilliseconds() === 0 && d.getSeconds() === 0;
  }

  function isHour(d) {
    return isMinute(d) && d.getMinutes() === 0;
  }

  function isDay(d) {
    return isHour(d) && d.getHours() === 0;
  }

  function isWeek(d, dayOfWeek) {
    return isDay(d) && d.getDay() === dayOfWeek;
  }

  function isMonth(d) {
    return isDay(d) && d.getDate() === 1;
  }

  function isYear(d) {
    return isMonth(d) && d.getMonth() === 0;
  }

  function isDate(obj) {
    return !isNaN(toDate(obj)) && toStr(obj).length >= 6;
  }

  function allZeros(data) {
    var i, j, d;
    for(i = 0; i < data.length; i++) {
      d = data[i].data;
      for(j = 0; j < d.length; j++) {
        if(d[j][1] != 0) {
          return false;
        }
      }
    }
    return true;
  }

  function detectDiscrete(series) {
    var i, j, data;
    for(i = 0; i < series.length; i++) {
      data = toArr(series[i].data);
      for(j = 0; j < data.length; j++) {
        if(!isDate(data[j][0])) {
          return true;
        }
      }
    }
    return false;
  }

  function processSeries(chart) {
    return chart.rawData;
  }

  function processSimple(chart) {
    return chart.rawData;
  }

  function processLineData(chart) {
    return processSeries(chart, "datetime");
  }

  function processColumnData(chart) {
    return processSeries(chart, "string");
  }

  function processBarData(chart) {
    return processSeries(chart, "string");
  }

  function processAreaData(chart) {
    return processSeries(chart, "datetime");
  }

  function processScatterData(chart) {
    return processSeries(chart, "number");
  }

  function createChart(chartType, chart, element, dataSource, opts, processData) {
    var elementId;
    if(typeof element === "string") {
      elementId = element;
      element = document.getElementById(element);
      if(!element) {
        throw new Error("No element with id " + elementId);
      }
    }

    setText(element, 'Loading...');

    chart.element = element;
    opts = merge(YazdaChart.options, opts || {});
    chart.options = opts;
    chart.dataSource = dataSource;

    // getters
    chart.getElement = function() {
      return element;
    };
    chart.getDataSource = function() {
      return chart.dataSource;
    };
    chart.getData = function() {
      return chart.data;
    };
    chart.getOptions = function() {
      return chart.options;
    };
    chart.getChartObject = function() {
      return chart.chart;
    };
    chart.getAdapter = function() {
      return chart.adapter;
    };

    var callback = function() {
      chart.data = processData(chart);
      renderChart(chartType, chart);
    };

    // functions
    chart.updateData = function(dataSource, options) {
      chart.dataSource = dataSource;
      if(options) {
        chart.options = merge(YazdaChart.options, options);
      }
      fetchDataSource(chart, callback, dataSource);
    };
    chart.setOptions = function(options) {
      chart.options = merge(YazdaChart.options, options);
      chart.redraw();
    };
    chart.redraw = function() {
      fetchDataSource(chart, callback, chart.rawData);
    };
    chart.refreshData = function() {
      if(typeof dataSource === "string") {
        // prevent browser from caching
        var sep = dataSource.indexOf("?") === -1 ? "?" : "&";
        var url = dataSource + sep + "_=" + (new Date()).getTime();
        fetchDataSource(chart, callback, url);
      }
    };
    chart.stopRefresh = function() {
      if(chart.intervalId) {
        clearInterval(chart.intervalId);
      }
    };
    chart.toImage = function() {
      if(chart.adapter === "chartjs") {
        return chart.chart.toBase64Image();
      } else {
        return null;
      }
    };

    YazdaChart.charts[element.id] = chart;

    fetchDataSource(chart, callback, dataSource);

    if(opts.refresh) {
      chart.intervalId = setInterval(function() {
        chart.refreshData();
      }, opts.refresh * 1000);
    }
  }

  // define classes

  YazdaChart = {
    LineChart:    function(element, dataSource, options) {
      createChart("LineChart", this, element, dataSource, options, processLineData);
    },
    PieChart:     function(element, dataSource, options) {
      createChart("PieChart", this, element, dataSource, options, processSimple);
    },
    DonutChart:   function(element, dataSource, options) {
      createChart("DonutChart", this, element, dataSource, options, processSimple);
    },
    ColumnChart:  function(element, dataSource, options) {
      createChart("ColumnChart", this, element, dataSource, options, processColumnData);
    },
    BarChart:     function(element, dataSource, options) {
      createChart("BarChart", this, element, dataSource, options, processBarData);
    },
    AreaChart:    function(element, dataSource, options) {
      createChart("AreaChart", this, element, dataSource, options, processAreaData);
    },
    ScatterChart: function(element, dataSource, options) {
      createChart("ScatterChart", this, element, dataSource, options, processScatterData);
    },
    charts:       {},
    configure:    function(options) {
      for(var key in options) {
        if(options.hasOwnProperty(key)) {
          config[key] = options[key];
        }
      }
    },
    eachChart:    function(callback) {
      for(var chartId in YazdaChart.charts) {
        if(YazdaChart.charts.hasOwnProperty(chartId)) {
          callback(YazdaChart.charts[chartId]);
        }
      }
    },
    options:      {},
    adapters:     adapters,
    createChart:  createChart
  };

  if(typeof module === "object" && typeof module.exports === "object") {
    module.exports = YazdaChart;
  } else {
    window.YazdaChart = YazdaChart;
  }
}(window));
