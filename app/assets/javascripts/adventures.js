// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).on('turbolinks:load', function() {
  $('.best_in_place').best_in_place();

  $('input.dynamic-selectize-control').selectize({
    plugins:     ['remove_button'],
    valueField:  'name',
    labelField:  'name',
    searchField: 'name',
    delimiter:   ',',
    persist:     false,
    create:      true,
    load:        function(query, callback) {
      if(!query.length) {
        return callback();
      }
      $.ajax({
        url:     '/tags?context=' + this.$input.data('context') + '&q=' + encodeURIComponent(query),
        type:    'GET',
        error:   function() {
          callback();
        },
        success: function(res) {
          callback(res);
        }
      });
    },
    render:      {
      option: function(item, escape) {
        return '<p>' + escape(item.name) + '</p>';
      }
    }
  });

  $('.best_in_place[data-bip-attribute="track_time"]').on('ajax:success', function() {
    $(this).parent().addClass('enabled');
    $(this).replaceWith('<span class="text-center">YES</span>');
  });
  $('.best_in_place[data-bip-attribute="attended"]').on('ajax:success', function() {
    $(this).parent().toggleClass('enabled');
  });
});
