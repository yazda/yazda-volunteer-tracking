// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).on('turbolinks:load', function() {
  $('#club_context_list').selectize({
    plugins:     ['remove_button'],
    valueField:  'name',
    labelField:  'name',
    searchField: 'name',
    delimiter:   ',',
    persist:     false,
    create:      true,
    maxItems:    5,
    load:        function(query, callback) {
      if(!query.length) {
        return callback();
      }
      $.ajax({
        url:     '/tags?context=contexts&q=' + encodeURIComponent(query),
        type:    'GET',
        error:   function() {
          callback();
        },
        success: function(res) {
          callback(res);
        }
      });
    },
    render:      {
      option: function(item, escape) {
        return '<p>' + escape(item.name) + '</p>';
      }
    }
  });

  var volunteersSwitch = $('#club_volunteers_submit_hours');
  var emailText = $('.email-text-container');

  if(!volunteersSwitch.is(':checked')) {
    emailText.hide();
  }

  volunteersSwitch.click(function() {
    emailText.toggle();
  });

  var links = $('.links');

  if($('.nested-fields').length > 4) {
    links.hide();
  } else {
    links.show();
  }

  links.on('cocoon:after-insert', function() {
    if($('.nested-fields').length > 4) {
      links.hide();
    } else {
      links.show();
    }
  });

  $(document).on('cocoon:after-remove', function(event, element) {
    element.remove();
    if($('.nested-fields').length > 4) {
      links.hide();
    } else {
      links.show();
    }
  })
});
