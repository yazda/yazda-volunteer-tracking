require 'yazda/api'
require Rails.root.join('lib', 'volunteer_tracking')

class AdventureSyncService
  attr_reader :user, :client, :adventures

  def initialize(user)
    endpoint = ENV['YAZDA_ENDPOINT']
    endpoint += '/v1' if endpoint.present?

    @user   = user
    @client = if ENV['YAZDA_ENDPOINT']
                Yazda::Api::Client.new(access_token: @user.access_token,
                                       endpoint:     endpoint)
              else
                Yazda::Api::Client.new(access_token: @user.access_token)
              end
  end

  def sync_club_adventures(club)
    return unless club.enabled?
    adventures = adventures_by_club(club.yazda_club_id)

    adventures['adventures'].each do |adventure|
      next if Adventure.find_by yazda_adventure_id: adventure['id']

      ActiveRecord::Base.transaction(requires_new: true) do
        a = create_adventure(adventure)

        a.update(track_time: true) if club.always_track_time?
      end
    end

    nil
  rescue Yazda::Api::APIError => e
    NewRelic::Agent.notice_error e
    'There was a problem syncing your adventures'
  end

  private

  def adventures_by_club(club_id)
    client.volunteer_tracking_adventures(club_id: club_id)
  end

  def create_adventure(adventure)
    club = Club.find_by(yazda_club_id: adventure['club']['id'])
    a    = Adventure.create(yazda_adventure_id:      adventure['id'],
                            name:                    adventure['name'],
                            address:                 adventure['address'],
                            adventure_type:          adventure['adventure_type'],
                            start_time:              adventure['start_time'],
                            end_time:                adventure['end_time'],
                            owner_name:              adventure['owner']['name'],
                            owner_profile_image_url: adventure['owner']['profile_image_url'],
                            club:                    club)

    # Owner's get an invite ID of 0
    create_volunteer a, adventure['owner'], club

    users = client.volunteer_tracking_users(ids: adventure['attendings'])

    users['users'].each do |user|
      create_volunteer a, user, club
    end

    a
  end

  def create_volunteer(adventure, user, club)
    u = User.find_or_create_by(yazda_user_id: user['id']) do |new_user|
      new_user.name              = user['name']
      new_user.profile_image_url = user['profile_image_url']
      new_user.email             = user['email']
    end

    ClubUser.find_or_create_by(user: u, club: club)

    Volunteer.create(adventure:     adventure,
                     yazda_user_id: user['id'],
                     user:          u,
                     club:          club)
  end
end
