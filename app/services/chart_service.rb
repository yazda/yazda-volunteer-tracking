class ChartService
  GROUP_BY_DATE = %w(day week month quarter year day_of_week day_of_month month_of_year)

  def initialize(club)
    @club = club
  end

  # Valid Options
  #
  # tags - (string separated by commans) filter by specific tags
  # top - (number) displays the top x adventures by hour
  # start_time - (date) when to start the report
  # end_time - (date) when to end the report
  # period - (day week month quarter year) how to group efforts
  # op - (count, hour) view by adventures count or sum of hours
  #
  def adventures(opts = {})
    adventures = Adventure.reportable
                   .where(club: club)
                   .in_date(opts[:start_time], opts[:end_time])
                   .filter(opts[:tags])

    @query = Adventure.where(id: adventures.pluck(:id))
               .joins(:volunteers)

    if opts[:top].present?
      @query = by_hour @query.group('name'), opts[:top].to_i

      {
        columns: @query.inject([]) do |ret, o|
          ret << [o[0], o[1].to_f]
          ret
        end
      }
    elsif opts[:period]
      @query = @query
                 .group_by_period(opts[:period], :start_time, permit: GROUP_BY_DATE)

      @query = if opts[:op] == 'count'
                 label = 'Count'
                 @query.count
               else
                 label = 'Hours'
                 @query.sum :hours
               end

      # get's time series row for user
      {
        x: 'x',
        columns:
          [
            @query.keys.unshift('x'),
            @query.values.unshift(label)
          ]
      }
    end
  end

  # Valid Options
  #
  # tags - (string separated by commans) filter by specific tags
  # top - (number) displays the top x adventures by hour
  # start_time - (date) when to start the report
  # end_time - (date) when to end the report
  # period - (day week month quarter year) how to group efforts
  # op - (count, hour) view by adventures count or sum of hours
  #
  def tags(opts = {})
    adventures = Adventure.reportable
                   .where(club: club)
                   .in_date(opts[:start_time], opts[:end_time])
                   .filter(opts[:tags])

    @query = Adventure.where(id: adventures.pluck(:id))
               .joins(:base_tags, :volunteers)
               .group('tags.id', 'tags.name')

    if opts[:top].present?
      @query = by_hour @query, opts[:top].to_i

      {
        columns: @query.inject([]) do |ret, o|
          ret << [o[0][1], o[1].to_f]
          ret
        end
      }
    elsif opts[:period]
      @query = @query
                 .group_by_period(opts[:period], :start_time, permit: GROUP_BY_DATE)

      @query = if opts[:op] == 'count'
                 @query.count
               else
                 @query.sum :hours
               end

      tags  = @query.keys.uniq { |key| key[0] }.map { |o| o[1] }
      dates = @query.keys.uniq { |key| key[2] }.map { |o| o[2] }.unshift 'x'

      # get's time series row for user
      {
        x:       'x',
        columns: tags.map do |tag|
          @query.select { |obj| obj[1] == tag }.map { |o| o[1].to_f }.unshift tag
        end.unshift(dates)
      }
    end
  end

  # Valid Options
  #
  # tags - (string separated by commans) filter by specific tags
  # top - (number) displays the top x adventures by hour
  # start_time - (date) when to start the report
  # end_time - (date) when to end the report
  # period - (day week month quarter year) how to group efforts
  #
  def volunteers(opts = {})
    adventures = Adventure.reportable
                   .where(club: club)
                   .in_date(opts[:start_time], opts[:end_time])
                   .filter(opts[:tags])

    @query = Volunteer.where(adventure_id: adventures.pluck(:id))
               .joins(:adventure).group(:user_id)

    if opts[:top].present?
      @query = by_hour @query, opts[:top].to_i

      {
        columns: @query.inject([]) do |ret, o|
          ret << [User.find(o[0]).name, o[1].to_f]
          ret
        end
      }
    elsif opts[:period].present?
      @query   = @query
                   .group_by_period(opts[:period], :start_time, permit: GROUP_BY_DATE)
                   .sum(:hours)

      # get's time series unique user
      user_ids = @query.keys.uniq { |key| key[0] }.map { |o| o[0] }
      dates    = @query.keys.uniq { |key| key[1] }.map { |o| o[1] }.unshift 'x'
      users    = User.where(id: user_ids)

      # get's time series row for user
      {
        'x':     'x',
        columns: users.map do |user|
          @query.select { |obj| obj[0] == user.id }.map { |o| o[1].to_f }.unshift user.name
        end.unshift(dates)
      }
    end
  end

  private

  attr_reader :club

  def by_hour(query, limit)
    query.order('SUM(COALESCE(hours, 0)) desc').limit(limit).sum(:hours)
  end
end
