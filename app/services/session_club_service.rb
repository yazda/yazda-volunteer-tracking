class SessionClubService
  attr_reader :user

  def initialize(user)
    @user = user
  end

  def enabled_clubs(ids)
    Club.where(yazda_club_id: ids, club_status: :enabled)
  end

  def selected_club(session, club = nil)
    selected_club = user.enabled_clubs.find_by(id: club&.id) ||
      user.enabled_clubs.find_by(id: session[:selected_club_id]) ||
      user.enabled_clubs.first

    session[:selected_club_id] = selected_club&.id
    selected_club
  end
end
