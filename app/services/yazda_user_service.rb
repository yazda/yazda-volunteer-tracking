class YazdaUserService
  attr_reader :controller, :user

  delegate :reset_session, to: :controller

  def initialize(controller, user)
    @user       = user
    @controller = controller
  end

  def save_user(session, auth_hash)
    user.access_token      = auth_hash.credentials.token
    user.profile_image_url = auth_hash.info.profile_image_url
    user.name              = auth_hash.info.name
    user.email             = auth_hash.info.email

    if user.save
      reset_session

      session[:yazda_user_id] = auth_hash.uid
      session[:clubs]         = auth_hash.info.clubs.map do |club|
        create_club_and_club_user club if club['role'] != 'member'

        { id: club['id'], role: club['role'], name: club['name'] }
      end
    else
      return false
    end

    # rubocop:disable Rails/SkipsModelValidations
    Volunteer.where(yazda_user_id: user.yazda_user_id, user_id: nil)
      .update_all(user_id: user.id)
    # rubocop:enable Rails/SkipsModelValidations
  end

  private

  def create_club_and_club_user(club)
    c = Club.find_or_create_by! yazda_club_id: club['id'] do |new_club|
      new_club.name             = club['name']
      new_club.website          = club['website']
      new_club.banner_image_url = club['banner_image_url']
      new_club.profile_image_url = club['profile_image_url']
    end

    c.update banner_image_url: club['banner_image_url'] if c.banner_image_url.blank?
    c.update profile_image_url: club['profile_image_url'] if c.profile_image_url.blank?

    club_user = ClubUser.find_or_create_by user: user, club: c

    club_user.update(role: club['role'])
  end
end
