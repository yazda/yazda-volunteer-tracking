class VolunteerService
  attr_reader :user

  def initialize(user)
    @user = user
  end

  def create_new_volunteer(volunteer)
    saved          = false
    new_user       = User.find_by(email: volunteer.user.email)
    volunteer.user = new_user if new_user.present?

    Volunteer.transaction do
      saved = volunteer.save && volunteer.user.club_users.find_or_create_by(club: user.selected_club)
    end

    saved
  end
end
