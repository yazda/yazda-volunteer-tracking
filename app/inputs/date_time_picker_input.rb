## app/inputs/date_time_picker_input.rb
class DateTimePickerInput < SimpleForm::Inputs::Base
  def input
    date = template.content_tag(:div, class: 'medium-6 small-12 columns') do
      template.concat @builder.text_field(:"#{attribute_name}_date",
                                          date_html_options)
    end

    time = template.content_tag(:div, class: 'medium-6 small-12 columns') do
      @builder.text_field(:"#{attribute_name}_time", time_html_options)
    end

    (date << time).html_safe
  end

  def date_html_options
    input_html_options.merge(
      class:    'form-date-picker',
      readonly: true,
      data:     {
        format:        'j M Y',
        init_set:      false,
        large_mode:    'true',
        large_default: 'true',
        modal:         'true',
        min_year:      '2010',
        theme:         'dark'
      }
    )
  end

  def time_html_options
    input_html_options.merge(
      class:    'form-time-picker',
      readonly: true
    )
  end
end
