## app/inputs/date_picker_input.rb
class DatePickerInput < SimpleForm::Inputs::Base
  def input
    template.content_tag(:div, class: 'date form-date') do
      template.concat @builder.text_field(attribute_name, input_html_options)
      template.concat span_remove
      template.concat span_table
    end
  end

  def input_html_options
    super.merge(
      class:    'form-date-picker',
      readonly: true,
      data:     {
        format:        'j M Y',
        init_set:      false,
        large_mode:    'true',
        large_default: 'true',
        modal:         'true',
        min_year:      '2010',
        theme:         'dark'
      }
    )
  end

  def span_remove
    template.content_tag(:span, class: 'input-group-addon') do
      template.concat icon_remove
    end
  end

  def span_table
    template.content_tag(:span, class: 'input-group-addon') do
      template.concat icon_table
    end
  end

  def icon_remove
    "<i class='glyphicon glyphicon-remove'></i>".html_safe
  end

  def icon_table
    "<i class='glyphicon glyphicon-th'></i>".html_safe
  end

end
