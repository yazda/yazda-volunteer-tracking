module ChartsHelper
  def column_chart url, options = {}
    build_chart 'BarChart', url, options
  end

  alias bar_chart column_chart

  def pie_chart url, options = {}
    build_chart 'PieChart', url, options
  end

  def donut_chart url, options = {}
    build_chart 'DonutChart', url, options
  end

  def line_chart url, options = {}
    build_chart 'LineChart', url, options
  end

  def area_chart url, options = {}
    build_chart 'AreaChart', url, options
  end

  def scatter_chart url, options = {}
    build_chart 'ScatterChart', url, options
  end

  private

  def build_chart(type, url, options)
    @chart_id  ||= 0
    height     = options.delete(:height) || "300px"
    width      = options.delete(:width) || "100%"
    element_id = options.delete(:id) || "chart-#{@chart_id += 1}"
    defer      = !!options.delete(:defer)
    createjs   = "new YazdaChart.#{type}(#{element_id.to_json}, #{url.to_json}, #{options.to_json});"

    if defer
      js = <<JS
<div id=#{element_id} style="height: #{height}; width: #{width}; text-align: center"></div>
<script type="text/javascript">
  (function() {
    var createChart = function() { #{createjs} };
    if (window.addEventListener) {
      window.addEventListener("load", createChart, true);
    } else if (window.attachEvent) {
      window.attachEvent("onload", createChart);
    } else {
      createChart();
    }
  })();
</script>
JS
    else
      js = <<JS
<div id=#{element_id} style="height: #{height}; width: #{width}; text-align: center"></div>
<script type="text/javascript">
  #{createjs}
</script>
JS
    end

    js.html_safe
  end
end
