module ApplicationHelper
  def auto_suggestions(context)
    ActsAsTaggableOn::Tagging
      .includes(:tag).where(context: context).uniq.pluck(:name)
  end

  def sort_link(column, title = nil)
    title ||= column.titleize

    direction = column == sort_name && sort_direction.include?('asc') ? 'desc' : 'asc'
    icon      = sort_direction == 'asc' ? 'ion-chevron-up' : 'ion-chevron-down'
    icon      = column == sort_name ? icon : ''
    link_to safe_join(["#{title} <span><i class='#{icon}'></i></span>".html_safe]),
            params.permit(:q, :filter).merge(sort: column, direction: direction)
  end

  def yazda_image_tag(image, opts = {})
    image_tag (image || 'badges/placeholder-avatar.svg'), opts
  end
end
