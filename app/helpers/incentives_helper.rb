module IncentivesHelper
  def incentive_percentage(club, user = current_user)
    incentive = club.max_incentive
    hours     = user.year_to_date_volunteers_for_club(club).sum(:hours)

    val = (hours / incentive.hours) * 100
    val = 100 if val > 100

    number_to_percentage(val, precision: 0) if incentive.present?
  end

  def incentive?(club, user = current_user)
    club.incentives.count > 0 && user.year_to_date_volunteers_for_club(club).exists?
  end

  def hours_for_incentive(club, user = current_user)
    # TODO change this to be next incentive
    incentive         = club.incentives.next_incentive_for_hours(user.year_to_date_volunteers_for_club(club))
    volunteered_hours = user.year_to_date_volunteers_for_club(club).sum(:hours)

    return unless incentive.present?

    hours = incentive.hours - volunteered_hours

    (hours >= 1 ? "#{hours} <span>to go</span>" : '<span>Complete!</span>').html_safe
  end

  def incentive_completed(incentive, club, user = current_user)
    hours = user.year_to_date_volunteers_for_club(club).sum(:hours)

    hours >= incentive.hours
  end

  def pip_for(incentive, club, user = current_user)
    max_incentive = club.max_incentive
    percentage    = (BigDecimal.new(incentive.hours) / BigDecimal.new(max_incentive.hours)) * 100

    complete  = incentive_completed(incentive, club, user) ? 'complete' : ''
    checkmark = "<span><i class='ion-checkmark-round' title='Milestone complete!'></i></span>"
    title     = complete.present? ? '' : "<span><strong>#{incentive.hours - current_user.hours_for_club(club)}</strong> to go</span>"

    pip       = <<-HTML
      <div class="pip #{complete}"
           style="left: #{number_to_percentage(percentage, precision: 0)}">
        <span data-tooltip aria-haspopup="true" class="has-tip"
              data-disable-hover="false" tabindex="1"
              title="<h6><strong>#{incentive.hours} Hours</strong>#{title}#{complete.present? ? checkmark : ''}</h6> #{incentive.description_markdown}"
              data-template-classes="incentive-tooltip complete"
              data-allow-html="true"><i class="ion-disc"></i></span>
      </div>
    HTML

    pip.html_safe
  end
end
