class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      arel_table = User.arel_table
      volunteers = Volunteer.arel_table
      adventures = Adventure.arel_table

      volunteer_efforts = arel_table
                            .join(volunteers, Arel::Nodes::OuterJoin)
                            .on(arel_table[:id].eq(volunteers[:user_id])
                                  .and(volunteers[:club_id].eq(user.selected_club.id)))
                            .join(adventures, Arel::Nodes::OuterJoin)
                            .on(volunteers[:adventure_id].eq(adventures[:id]))
                            .join_sources


      scope.joins(volunteer_efforts)
    end
  end

  def create?
    # TODO: actually verify this
    true
  end

  def update?
    record.club_users.exists? club: user.selected_club
  end

  alias show? update?
end
