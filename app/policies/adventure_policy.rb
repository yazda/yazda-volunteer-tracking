class AdventurePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      user.selected_club.adventures.where.not(reportable_status: :archived)
        .joins(:volunteers).group('adventures.id')
    end
  end

  def index?
    user.selected_club.present?
  end

  def new?
    user.selected_club.present?
  end

  alias create? new?

  def edit?
    yazda_club_id = record.club.yazda_club_id

    user.leader_clubs.map { |c| c['id'] }.include?(yazda_club_id) ||
      user.id == record.user.id
  end

  alias update? edit?
  alias track_time? edit?
  alias set_time_for_volunteers? edit?
end
