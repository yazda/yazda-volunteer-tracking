class ClubPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      ids = user.admin_clubs&.map { |c| c['id'] }

      scope.where(yazda_club_id: ids)
    end
  end

  def edit?
    user.admin_clubs&.map { |c| c['id'] }.include? record.yazda_club_id
  end

  alias update? edit?
  alias enable? edit?
  alias disable? edit?
end
