class ReportItemPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def create?
    user.selected_club.present? &&
      record.report.club.id == user.selected_club.id &&
      user.admin_clubs.map { |c| c['id'] }.include?(user.selected_club.yazda_club_id)
  end

  alias destroy? create?
  alias update? create?
end
