class EmailPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      ids = user.admin_clubs&.map { |c| c['id'] }

      scope.where(yazda_club_id: ids)
    end
  end

  def create?
    user.selected_club.present?
  end

  alias update? create?
end
