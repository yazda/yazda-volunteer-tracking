class VolunteerPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def index?
    user.selected_club.present?
  end

  def update?
    user.id == record.user.id || user.selected_club&.id == record.club.id
  end

  alias create? update?

  def permitted_attributes_for_update
    p = [:hours]

    p << :track_time if user.selected_club&.id == record.club.id
    p << :attended if user.selected_club&.id == record.club.id
    p
  end
end
