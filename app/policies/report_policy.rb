class ReportPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def index?
    user.selected_club.present?
  end

  def create?
    index? && record.club.id == user.selected_club.id &&
      user.admin_clubs.map { |c| c['id'] }.include?(user.selected_club.yazda_club_id)
  end

  def show?
    record.published? || user.selected_club&.id == record.club.id
  end

  alias edit? create?
  alias update? create?
  alias destroy? create?
end
