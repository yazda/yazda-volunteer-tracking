class SyncClubAdventuresWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'scheduled'

  def perform(club_id)
    club = Club.find club_id
    user = club.users.where.not(access_token: nil).first

    AdventureSyncService.new(user).sync_club_adventures(club)
  end
end
