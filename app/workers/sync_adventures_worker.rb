class SyncAdventuresWorker
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: 'scheduled'

  def perform
    Club.enabled.find_each do |club|
      SyncClubAdventuresWorker.perform_async club.id
    end
  end
end
