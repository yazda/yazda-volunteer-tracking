class AdventureSubmitHoursWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'scheduled'

  def perform(adventure_id)
    adventure = Adventure.find adventure_id

    adventure.volunteers.where(track_time: false).find_each do |volunteer|
      VolunteerMailer.submit_hours(volunteer).deliver_later
      VolunteerMailer.follow_up_submit_hours(volunteer).deliver_later(wait: 1.day)
      VolunteerMailer.follow_up_submit_hours(volunteer).deliver_later(wait: 3.days)
      VolunteerMailer.follow_up_submit_hours(volunteer).deliver_later(wait: 1.week)
    end
  end
end
