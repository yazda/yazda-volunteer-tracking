class SendEmailWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'mailers'

  def perform(email_id, club_id, params)
    email = Email.find email_id
    club  = Club.find club_id

    UserPolicy::Scope.new(nil, club.users).scope
      .filter(params['filter'], club)
      .search(params['q'])
      .in_date(params['start_date'], params['end_date'])
      .hours(params['hours'])
      .group('users.id').find_each do |u|

      # TODO: email individual users
      VolunteerMailer.mail_volunteer(u, email).deliver_later
    end
  end
end
