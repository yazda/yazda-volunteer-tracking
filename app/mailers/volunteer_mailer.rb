class VolunteerMailer < ApplicationMailer
  def submit_hours(volunteer)
    @volunteer = volunteer

    mail(to:      @volunteer.user.email,
         subject: "Submit your Hours - #{@volunteer.club.name}")
  end

  def mail_volunteer(user, email)
    @user  = user
    @email = email

    mail(to: @user.email, subject: email.subject)
  end

  def follow_up_submit_hours(volunteer)
    return if volunteer.hours.present?

    @volunteer = volunteer

    mail(to:      @volunteer.user.email,
         subject: "Reminder - Submit your Hours - #{@volunteer.club.name}")
  end

  def incentive_achieved(volunteer, incentive)
    @volunteer = volunteer
    @incentive = incentive

    mail(to:      @volunteer.user.email,
         subject: "Congratulations on your hard work")
  end
end
