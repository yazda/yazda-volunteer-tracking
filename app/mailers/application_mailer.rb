class ApplicationMailer < ActionMailer::Base
  add_template_helper ApplicationHelper
  default from: 'Yazda - Volunteer Management <no-reply@yazdaapp.com>'
  layout 'mailer'
end
