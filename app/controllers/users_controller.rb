class UsersController < ApplicationController
  def create
    @user = User.find_by(email: user_params[:email])
    @user = User.new(user_params) unless @user.present?
    @user.club_users.build(club: current_user.selected_club)
    authorize @user

    context = "club_#{current_user.selected_club.id}_skills".to_sym
    @user.set_tag_list_on(context, params[:user][context])

    respond_to do |format|
      flash[:notice] = 'User saved!' if @user.save

      format.js {}
    end
  end

  def show
    @user = User.find(params[:id])
    authorize @user

    @volunteers = @user.volunteers.joins(:adventure)
                    .where(club: current_user.selected_club)
                    .order('adventures.end_time desc')
  end

  def update
    @user = User.find(params[:id])
    authorize @user

    context = current_user.selected_club.skills_context
    @user.send("#{context}=", params[:user][context])

    @user.update(user_params)
  end

  protected

  def user_params
    params.require(:user).permit(:email, :name)
  end
end
