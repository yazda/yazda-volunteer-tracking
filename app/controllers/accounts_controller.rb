class AccountsController < ApplicationController
  skip_after_action :verify_authorized

  def show
    @clubs = policy_scope Club
  end

  def update
    current_user.update(account_params)
    @clubs = policy_scope Club

    render :show
  end

  private

  def account_params
    params.require(:user).permit(:monday_morning, :monday_afternoon,
                                 :monday_evening, :tuesday_morning,
                                 :tuesday_afternoon, :tuesday_evening,
                                 :wednesday_morning, :wednesday_afternoon,
                                 :wednesday_evening, :thursday_morning,
                                 :thursday_afternoon, :thursday_evening,
                                 :friday_morning, :friday_afternoon,
                                 :friday_evening, :saturday_morning,
                                 :saturday_afternoon, :saturday_evening,
                                 :sunday_morning, :sunday_afternoon,
                                 :sunday_evening, :skill_list)
  end
end
