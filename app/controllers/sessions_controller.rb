class SessionsController < ApplicationController
  skip_before_action :require_login, only: [:create, :failure]
  skip_before_action :redirect_if_unsubmitted_volunteer_hours,
                     only: [:create, :failure]

  skip_after_action :verify_authorized

  def create
    @user = User.find_by(yazda_user_id: auth_hash.uid) ||
      User.find_by(email: auth_hash.info.email)
    @user = User.new(email:         auth_hash.info.email,
                     yazda_user_id: auth_hash.uid) unless @user

    if YazdaUserService.new(self, @user).save_user(session, auth_hash)
      auto_login @user
      redirect_back_or_to root_path
    else
      redirect_to root_path, alert: 'Failed to login.'
    end
  end

  def failure
  end

  def destroy
    logout

    redirect_to root_path
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end
