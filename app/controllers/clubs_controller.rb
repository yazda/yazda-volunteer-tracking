class ClubsController < ApplicationController
  def edit
    @club        = Club.find(params[:id])
    @hours_count = @club.volunteers.sum(:hours)
    authorize @club
  end

  def update
    @club = Club.find(params[:id])
    authorize @club

    if @club.update update_club_params
      redirect_to account_path
    else
      render :edit
    end
  end

  def enable
    @club = Club.find(params[:id])
    authorize @club

    if @club.allow_enabled
      @club.enabled!
    else
      flash.now[:alert] = 'This club is not allowed to be enabled. Email us at info@yazdaapp.com for beta access'
    end

    render :edit
  end

  def disable
    @club = Club.find(params[:id])
    authorize @club

    @club.disabled!
    render :edit
  end

  private

  def update_club_params
    params.require(:club).permit(:context_list, :volunteers_submit_hours,
                                 :submit_hours_email_text, :always_track_time,
                                 incentives_attributes: [:id, :hours,
                                                         :description,
                                                         :_destroy])
  end
end
