class VolunteersController < ApplicationController
  helper_method :sort_name, :sort_direction

  skip_before_action :redirect_if_unsubmitted_volunteer_hours

  def index
    authorize Volunteer
    @email = Email.new
    @user  = User.new

    @volunteers = policy_scope(current_user.selected_club.users)
                    .includes(:skills)
                    .filter(params[:filter], current_user.selected_club)
                    .in_date(params[:start_date], params[:end_date])
                    .hours(params[:hours])
                    .search(params[:q]).group('users.id')
                    .order("#{sort_column} #{sort_direction}")

    respond_to do |format|
      format.html { @volunteers = @volunteers.page(params[:page]) }
      format.csv do
        content_disposition = "attachment; filename='volunteers-#{DateTime.current.to_formatted_s(:short)}.csv"

        response.headers['Content-Type']        = 'text/csv'
        response.headers['Content-Disposition'] = content_disposition
      end
    end
  end

  def update
    @volunteer = Volunteer.find(params[:id])
    authorize @volunteer

    respond_to do |format|
      if @volunteer.update(permitted_attributes(@volunteer))
        format.html do
          flash[:notice] = 'Thank you for submitting your hours'
          redirect_to root_path
        end
      else
        format.html { render 'home/index' }
      end

      format.json { respond_with_bip(@volunteer) }
    end
  end

  def create
    @user      = current_user.selected_club.volunteers.build(volunteer_params)
    @user.club = current_user.selected_club
    authorize @user

    respond_to do |format|
      if VolunteerService.new(current_user).create_new_volunteer @user
        format.html do
          redirect_to(request.referer || adventure_path(volunteer_params[:adventure_id]))
        end
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end

      format.js {}
    end
  end

  protected

  def volunteer_params
    params.require(:volunteer)
      .permit(:adventure_id, :hours, :attended,
              user_attributes: [:email, :name])
  end

  def sortable_columns
    %w(name last_attended hours)
  end

  def sort_name
    sortable_columns.include?(params[:sort]) ? params[:sort] : 'last_attended'
  end

  def sort_column
    case sort_name
    when 'hours'
      'SUM(COALESCE(volunteers.hours, 0))'
    when 'last_attended'
      'MAX(adventures.end_time)'
    when 'name'
      'users.name'
    end
  end

  def sort_direction
    d = params[:direction]&.include?('asc') ? params[:direction] : 'desc'
    "#{d} NULLS LAST"
  end
end
