class ReportItemsController < ApplicationController
  def create
    report       = Report.friendly.find params[:report_id]
    @report_item = report.report_items.build create_params

    authorize @report_item

    if @report_item.save
      respond_to do |format|
        format.js {}
      end
    else
      redirect_to edit_report_path(report), alert: 'Error adding report item'
    end
  end

  def update
    @report_item = ReportItem.find(params[:id])
    authorize @report_item

    @report_item.update update_params

    respond_to do |format|
      format.js {}
    end
  end

  def destroy
    @report_item = ReportItem.find(params[:id])
    authorize @report_item

    @report_item.destroy

    respond_to do |format|
      format.js {}
    end
  end

  private

  def create_params
    params.require(:report_item).permit(:item_type)
  end

  def update_params
    params.require(:report_item)
      .permit(:content, :width, :top, :url, :period, :op, :position)
  end
end
