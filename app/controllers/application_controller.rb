class ApplicationController < ActionController::Base
  include Pundit

  protect_from_forgery with: :exception
  before_action :require_login
  before_action :redirect_if_unsubmitted_volunteer_hours
  before_action :set_selected_club

  after_action :verify_authorized, except: :index

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  protected

  def user_not_authorized
    flash[:alert] = 'You are not authorized to perform this action.'
    redirect_to(request.referer || root_path)
  end

  def not_authenticated
    redirect_to main_app.root_path
  end

  def redirect_if_unsubmitted_volunteer_hours
    @volunteer = current_user&.unfilled_volunteers&.first if logged_in?

    redirect_to root_path if @volunteer.present? && request.path != root_path
  end

  def set_selected_club
    if logged_in?
      current_user.imported_clubs = session[:clubs]

      ids = current_user.leader_clubs&.map { |c| c['id'] }

      service = SessionClubService.new(current_user)

      current_user.enabled_clubs = service.enabled_clubs ids
      current_user.selected_club = service.selected_club session
    end
  end

  def browser_timezone
    cookies['browser.timezone']
  end
end
