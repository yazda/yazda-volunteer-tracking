class TagsController < ApplicationController
  skip_after_action  :verify_authorized

  def index
    @tags = ActsAsTaggableOn::Tagging.joins(:tag)

    @tags = @tags.where(context: params[:context]) if params[:context].present?
    @tags = @tags.where('LOWER(name) ILIKE LOWER(:q)', q: "%#{params[:q]}%") if params[:q].present?

    @tags = @tags.uniq.page(params[:page]).pluck(:name)

    render json: @tags.map { |t| { name: t } }
  end
end
