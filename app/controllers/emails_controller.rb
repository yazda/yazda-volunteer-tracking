class EmailsController < ApplicationController
  def create
    @email = current_user.selected_club.emails.build(email_params)
    authorize @email

    respond_to do |format|
      if @email.save
        SendEmailWorker.perform_async @email.id,
                                      current_user.selected_club.id,
                                      search_params.to_h
        # TODO: email individual users
      end

      format.js {}
    end
  end

  protected

  def email_params
    params.require(:email).permit(:subject, :message)
  end

  def search_params
    params.permit(:filter, :q, :start_date, :end_date, :hours)
  end
end
