class AdventuresController < ApplicationController
  helper_method :sort_name, :sort_direction

  def index
    authorize Adventure
    @adventures = policy_scope(Adventure)
                    .in_date(params[:start_date], params[:end_date])
                    .filter(params[:filter])
                    .search(params[:q])
                    .order("#{sort_column} #{sort_direction}")

    @club = current_user.selected_club

    respond_to do |format|
      format.html { @adventures = @adventures.page(params[:page]) }
      format.csv do
        content_disposition = "attachment; filename='adventures-#{DateTime.current.to_formatted_s(:short)}.csv"

        response.headers['Content-Type']        = 'text/csv'
        response.headers['Content-Disposition'] = content_disposition
      end
    end
  end

  def new
    @adventure = current_user.selected_club.adventures.build
    authorize @adventure
  end

  def create
    @adventure = current_user.selected_club.adventures.build(create_params)
    authorize @adventure

    @adventure.owner_name              = current_user.name
    @adventure.owner_profile_image_url = current_user.profile_image_url
    @adventure.volunteers.build(club:          current_user.selected_club,
                                user:          current_user,
                                attended:      true,
                                yazda_user_id: current_user.yazda_user_id)

    if @adventure.save
      redirect_to edit_adventure_path @adventure
    else
      render :new
    end
  end

  def edit
    @adventure = Adventure.find(params[:id])
    authorize @adventure

    @volunteers = @adventure.volunteers.includes(:user)

    @user = @adventure.volunteers.build
    @user.build_user
  end

  def update
    @adventure = Adventure.find(params[:id])
    authorize @adventure

    @adventure.club.context_list_underscore.each do |c|
      @adventure.send("#{c}=", params[:adventure][c])
    end

    if @adventure.update(update_params)
      redirect_to edit_adventure_path(@adventure)
    else
      render :edit
    end
  end

  def track_time
    @adventure = Adventure.find(params[:id])
    authorize @adventure

    @adventure.update(track_time: true)

    render :edit
  end

  def set_time_for_volunteers
    @adventure = Adventure.find(params[:id])
    authorize @adventure

    @adventure.volunteers.find_each do |volunteer|
      volunteer.update(hours: params[:volunteer][:hours])
    end

    render :edit, notice: 'Hours set for all Volunteers!'
  end

  private

  def create_params
    params.require(:adventure).permit(:name, :adventure_start_time,
                                      :adventure_start_date,
                                      :adventure_end_date,
                                      :adventure_end_time)
  end

  def update_params
    params.require(:adventure).permit(:reportable_status)
  end

  # TODO: put in the right spot
  def sortable_columns
    %w(name end_time hours reportable_status)
  end

  def sort_name
    sortable_columns.include?(params[:sort]) ? params[:sort] : 'end_time'
  end

  def sort_column
    if sort_name == 'hours'
      'SUM(COALESCE(volunteers.hours, 0))'
    else
      sort_name
    end
  end

  def sort_direction
    %w(asc desc).include?(params[:direction]) ? params[:direction] : 'desc'
  end
end
