class HomeController < ApplicationController
  skip_before_action :require_login

  skip_after_action  :verify_authorized

  def index
  end

  def select_club
    service = SessionClubService.new(current_user)

    current_user
      .selected_club = service.selected_club session,
                                             Club.find_by(id: params[:id])

    redirect_back(fallback_location: root_path)
  end
end
