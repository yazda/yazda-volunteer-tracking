class ChartsController < ApplicationController
  skip_after_action :verify_authorized

  def volunteer
    chart_service = ChartService.new(Club.find(params[:club_id]))

    render json: chart_service.volunteers(volunteer_params)
  end

  def tags
    # Gets all adventures grouping by adventure_id and tag
    chart_service = ChartService.new(Club.find(params[:club_id]))

    render json: chart_service.tags(tags_params)
  end

  def adventures
    chart_service = ChartService.new(Club.find(params[:club_id]))

    render json: chart_service.adventures(tags_params)
  end

  private

  def tags_params
    params.permit(:start_time, :end_time, :top, :period, :tags, :context, :op)
  end

  def volunteer_params
    params.permit(:start_time, :end_time, :top, :period, :tags, :context)
  end
end
