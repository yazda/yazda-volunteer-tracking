class ReportsController < ApplicationController
  layout 'public', only: :show

  skip_before_action :require_login, only: :show
  skip_after_action :verify_authorized

  def index
    authorize Report

    @reports = current_user.selected_club.reports
  end

  def create
    @report = current_user.selected_club.reports.build(create_params)
    authorize @report

    if @report.save
      redirect_to edit_report_path @report
    else
      render :new
    end
  end

  def new
    @report = current_user.selected_club.reports.build
    authorize @report
  end

  def show
    @report = Report.friendly.find params[:id]
    authorize @report
  end

  def edit
    @report = Report.friendly.find params[:id]
    authorize @report
  end

  def update
    @report = Report.friendly.find params[:id]
    authorize @report

    @report.status = case params[:commit]
                     when 'Publish'
                       :published
                     when 'Draft'
                       :draft
                     else
                       @report.status
                     end

    if @report.update(update_params)
      redirect_to reports_path, notice: 'Report saved!'
    else
      render :edit
    end
  end

  def destroy
    @report = Report.friendly.find params[:id]
    authorize @report

    @report.archived!
    redirect_to reports_path
  end

  private

  def create_params
    params.require(:report).permit(:name, :url, :description, :start_date,
                                   :end_date)
  end

  def update_params
    params.require(:report).permit(:name, :description, :start_date,
                                   :end_date, :tags_list, :primary_color,
                                   :secondary_color)
  end

end
