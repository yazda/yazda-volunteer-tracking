module YazdaMarkdown
  extend ActiveSupport::Concern

  included do
    def to_markdown(text)
      self.class.markdown.render(text)
    end
  end

  class_methods do
    def markdown
      Redcarpet::Markdown.new(Redcarpet::Render::HTML)
    end
  end
end
