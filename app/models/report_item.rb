class ReportItem < ApplicationRecord
  serialize :options, HashSerializer
  store_accessor :options, :tags, :url, :context, :start_time, :end_time,
                 :top, :period, :op, :primary_color, :secondary_color, :width

  belongs_to :report

  acts_as_list scope: :report

  enum item_type: [:donut_chart, :pie_chart, :bar_chart, :line_chart,
                   :area_chart, :scatter_chart, :heading, :text, :hr]

  validates :report, presence: true

  before_create :set_default_options

  def tags
    if options[:tags].present?
      report.tags_list.split(',').push(options[:tags]).join(',')
    else
      report.tags_list
    end
  end

  def start_time
    if options['start_time'].present?
      options['start_time']
    else
      report.start_date
    end
  end

  def end_time
    if options['end_time'].present?
      options['end_time']
    else
      report.end_date
    end
  end

  def primary_color
    options['primary_color'] || report.primary_color
  end

  def secondary_color
    options['secondary_color'] || report.secondary_color
  end

  def options= opts
    case item_type.to_sym
    when :donut_chart, :pie_chart, :bar_chart, :line_chart, :area_chart,
      :scatter_chart
      super ChartOptions.new(opts).to_h
    when :heading
      super TextOptions.new(opts).to_h
    when :text
      super TextOptions.new(opts).to_h
    when :hr
    end
  end

  def set_default_options
    case item_type.to_sym
    when :donut_chart, :pie_chart, :bar_chart
      self.options = { top: 10 }
    when :line_chart, :area_chart, :scatter_chart
      self.options = { period: 'week', op: 'hours' }
    else
      self.options = {}
    end
  end
end
