# == Schema Information
#
# Table name: users
#
#  id                  :integer          not null, primary key
#  yazda_user_id       :integer
#  profile_image_url   :string
#  name                :string
#  access_token        :string
#  crypted_password    :string
#  salt                :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  email               :string
#  monday_morning      :boolean          default("false"), not null
#  monday_afternoon    :boolean          default("false"), not null
#  monday_evening      :boolean          default("false"), not null
#  tuesday_morning     :boolean          default("false"), not null
#  tuesday_afternoon   :boolean          default("false"), not null
#  tuesday_evening     :boolean          default("false"), not null
#  wednesday_morning   :boolean          default("false"), not null
#  wednesday_afternoon :boolean          default("false"), not null
#  wednesday_evening   :boolean          default("false"), not null
#  thursday_morning    :boolean          default("false"), not null
#  thursday_afternoon  :boolean          default("false"), not null
#  thursday_evening    :boolean          default("false"), not null
#  friday_morning      :boolean          default("false"), not null
#  friday_afternoon    :boolean          default("false"), not null
#  friday_evening      :boolean          default("false"), not null
#  saturday_morning    :boolean          default("false"), not null
#  saturday_afternoon  :boolean          default("false"), not null
#  saturday_evening    :boolean          default("false"), not null
#  sunday_morning      :boolean          default("false"), not null
#  sunday_afternoon    :boolean          default("false"), not null
#  sunday_evening      :boolean          default("false"), not null
#

class User < ApplicationRecord
  acts_as_taggable_on :skills
  authenticates_with_sorcery!

  attr_accessor :imported_clubs, :selected_club, :enabled_clubs

  after_create :account_created

  scope :search, (lambda do |search|
    if search.present?
      where('users.name ILIKE ?', "%#{search}%")
    else
      all
    end
  end)

  scope :filter, (lambda do |filters, club|
    if filters.present?
      tagged_with(filters.split(','),
                  on:  club.skills_context,
                  any: true)
    else
      all
    end
  end)

  scope :hours, (lambda do |hours|
    if hours.present?
      group('users.id').having('sum(hours) >= ?', hours)
    else
      all
    end
  end)

  scope :in_date, (lambda do |start_date, end_date|
    return all unless start_date.present? || end_date.present?

    start_date = start_date.present? ? Date.parse(start_date) : Date.current
    end_date   = end_date.present? ? Date.parse(end_date) : Date.current

    where('volunteers.created_at BETWEEN ? and ?', start_date, end_date)
  end)

  has_many :club_users, dependent: :destroy
  has_many :clubs, through: :club_users
  has_many :volunteers
  has_many :adventures, through: :volunteers
  has_many :unfilled_volunteers,
           ->() { trackable.where(hours: nil) },
           class_name: 'Volunteer'

  validates :name, presence: true, length: { maximum: 255 }
  validates :access_token, allow_blank: true, length: { maximum: 255 }
  validates :email,
            presence: true,
            uniqueness:  { case_sensitive: false },
            format:      /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/
  validates :yazda_user_id,
            numericality: { only_integer:             true,
                            greater_than_or_equal_to: 1 },
            presence:     true,
            uniqueness:   true,
            if:           :yazda_user_id

  def admin_clubs
    return unless imported_clubs.present?

    imported_clubs.select do |club|
      club['role'] == 'admin' || club['role'] == 'owner'
    end
  end

  def leader_clubs
    return unless imported_clubs.present?

    imported_clubs.select do |club|
      club['role'] != 'member'
    end
  end

  def last_attended_for_club(club)
    adventures
      .where(club: club)
      .maximum(:end_time)&.strftime('%b %d \'%y')
  end

  def year_to_date_volunteers_for_club(club)
    volunteers.joins(:adventure, :club)
      .where(clubs:      { id: club.id },
             adventures: {
               start_time:        DateTime.current.beginning_of_year..DateTime.current
             })
  end

  def hours_for_club(club)
    year_to_date_volunteers_for_club(club).sum(:hours)
  end

  def all_skills(club)
    skills.to_a.concat(tags_on(club.skills_context))
  end

  def method_missing(method_sym, *arguments, &block)
    if method_sym.to_s =~ /context$/
      tags_on(method_sym).join(',')
    elsif method_sym.to_s =~ /context=$/
      method = method_sym.to_s.match(/^(\w*)/)
      set_tag_list_on(method[0], arguments.first)
    else
      super
    end
  end

  def respond_to_missing?(method, include_private = false)
    return true if method.to_s =~ /context$/ || method.to_s =~ /context=$/

    super
  end

  private

  def account_created
    # TODO: send email to created user asking them to setup their profile
  end
end
