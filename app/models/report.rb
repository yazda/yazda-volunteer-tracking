class Report < ApplicationRecord
  include FriendlyId

  friendly_id :url_candidates, use: :slugged

  belongs_to :club
  has_many :report_items, -> { order(position: :asc) }, dependent: :destroy

  enum status: [:draft, :published, :archived]

  validates :name, presence: true, length: { maximum: 255, minimum: 3 }
  validates :url, presence: true, length: { maximum: 255, minimum: 3 }
  validates :primary_color,
            allow_blank: true,
            format:      { with:    /\A([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})\z/,
                           message: 'must be a valid hex color' }

  validates :secondary_color,
            allow_blank: true,
            format:      { with:    /\A([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})\z/,
                           message: 'must be a valid hex color' }
  validates :start_date, presence: true
  validates :club_id, presence: true

  def hours
    adventures_ar.joins(:volunteers).sum(:hours)
  end

  def volunteers
    adventures_ar.joins(:volunteers).group(:user_id).distinct.count.count
  end

  def adventures
    adventures_ar.count
  end

  def url_candidates
    [
      :url,
      [:url, Date.current.year]
    ]
  end

  private

  def adventures_ar
    Adventure.reportable.in_date(start_date.to_s, end_date.to_s)
      .filter(tags_list)
      .where(club_id: club_id)
  end
end
