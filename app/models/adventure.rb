# == Schema Information
#
# Table name: adventures
#
#  id                      :integer          not null, primary key
#  yazda_adventure_id      :integer
#  name                    :string
#  start_time              :datetime
#  end_time                :datetime
#  owner_name              :string
#  owner_profile_image_url :string
#  track_time              :boolean          default("false")
#  club_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  address                 :string
#  adventure_type          :string
#  reportable_status       :integer          default("0")
#

class Adventure < ApplicationRecord
  acts_as_taggable

  attr_accessor :adventure_start_date, :adventure_start_time,
                :adventure_end_date, :adventure_end_time

  enum reportable_status: [:pending, :archived, :reportable]

  has_many :volunteers, dependent: :destroy
  belongs_to :club

  before_validation :set_start_time, :set_end_time
  after_update :submit_hours

  scope :in_date, (lambda do |start_date, end_date|
    return all unless start_date.present? || end_date.present?

    start_date = start_date.present? ? Date.parse(start_date) : Date.current
    end_date   = end_date.present? ? Date.parse(end_date) : Date.current

    where(end_time: start_date.beginning_of_day..end_date.end_of_day)
  end)

  scope :search, (lambda do |search|
    if search.present?
      where('name ILIKE ?', "%#{search}%")
    else
      all
    end
  end)

  scope :filter, (lambda do |filters|
    if filters.present?
      tagged_with(filters.split(','), any: true)
    else
      all
    end
  end)

  validates :club, presence: true
  validates :start_time, presence: true
  validates :end_time, presence: true
  validates :name, length: { maximum: 255 }, presence: true
  validates :owner_name, length: { maximum: 255 }, presence: true
  validates :owner_profile_image_url, length: { maximum: 255 }, presence: true
  validates :yazda_adventure_id,
            uniqueness:   true,
            numericality: {
              only_integer:             true,
              greater_than_or_equal_to: 1
            },
            if:           -> (obj) { obj.yazda_adventure_id.present? }

  def method_missing(method_sym, *arguments, &block)
    if method_sym.to_s =~ /context$/
      tags_on("club_#{club_id}_#{method_sym}").join(',')
    elsif method_sym.to_s =~ /context=$/
      method = method_sym.to_s.match(/^(\w*)/)
      set_tag_list_on("club_#{club_id}_#{method[0]}", arguments.first)
    else
      super
    end
  end

  def respond_to_missing?(method, include_private = false)
    return true if method.to_s =~ /context$/ || method.to_s =~ /context=$/

    super
  end

  def adventure_start_date
    return @adventure_start_date if @adventure_start_date.present?
    return start_time if start_time.present?
    Date.current
  end

  def adventure_start_time
    return @adventure_start_time if @adventure_start_time.present?
    return start_time if start_time.present?
    Date.current
  end

  def adventure_end_date
    return @adventure_end_date if @adventure_end_date.present?
    return end_time if end_time.present?
    Date.current
  end

  def adventure_end_time
    return @adventure_end_time if @adventure_end_time.present?
    return end_time if end_time.present?
    Date.current
  end

  private

  def submit_hours
    AdventureSubmitHoursWorker.perform_async(self.id) if track_time_changed?
  end

  def set_start_time
    return unless @adventure_start_date.present? || @adventure_start_time.present?

    self.start_time = DateTime.parse("#{adventure_start_date} #{adventure_start_time}")
  end

  def set_end_time
    return unless @adventure_end_date.present? || @adventure_end_time.present?

    self.end_time = DateTime.parse("#{adventure_end_date} #{adventure_end_time}")
  end
end
