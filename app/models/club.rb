# == Schema Information
#
# Table name: clubs
#
#  id                      :integer          not null, primary key
#  yazda_club_id           :integer
#  name                    :string
#  website                 :string
#  club_status             :integer          default("0")
#  volunteers_submit_hours :boolean          default("false")
#  allow_enabled           :boolean          default("false")
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  submit_hours_email_text :text
#  banner_image_url        :string
#  profile_image_url       :string
#  always_track_time       :boolean
#

class Club < ApplicationRecord
  include YazdaMarkdown

  acts_as_taggable_on :contexts

  # TODO: other status for payments
  enum club_status: [:disabled, :enabled]

  has_many :volunteers
  has_many :emails, dependent: :destroy
  has_many :adventures, dependent: :destroy
  has_many :volunteers, through: :adventures
  has_many :club_users, dependent: :destroy
  has_many :users, through: :club_users
  has_many :incentives, dependent: :destroy
  has_many :reports, dependent: :destroy

  accepts_nested_attributes_for :incentives,
                                reject_if:     :all_blank,
                                allow_destroy: true,
                                limit:         5

  validates_associated :incentives

  validates :name, length: { maximum: 255 }, presence: true
  validates :website, length: { maximum: 255 }, allow_blank: true
  validates :submit_hours_email_text,
            length:      { maximum: 10_000 },
            allow_blank: true
  validates :yazda_club_id,
            numericality: {
              only_integer:             true,
              greater_than_or_equal_to: 1
            },
            presence:     true,
            uniqueness:   true
  validate :validate_tags
  validate :incentive_length

  def max_incentive
    incentives.first
  end

  def context_list_underscore
    context_list.map { |c| "#{c}_context".tr(' ', '_').underscore }
  end

  def submit_hours_email_markdown
    to_markdown submit_hours_email_text.to_s
  end

  def skills_context
    "club_#{id}_skills_context"
  end

  private

  def validate_tags
    errors.add(:context_list, 'Maximum 5 tags') if context_list.length > 5
  end

  def incentive_length
    remaining_incentives = incentives.reject(&:marked_for_destruction?)

    errors.add :incentives, 'Maximum 5 Incentives' if remaining_incentives.size > 5
  end
end
