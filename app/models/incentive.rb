# == Schema Information
#
# Table name: incentives
#
#  id          :integer          not null, primary key
#  club_id     :integer
#  hours       :integer
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Incentive < ApplicationRecord
  include YazdaMarkdown

  belongs_to :club

  default_scope ->() { order(hours: :desc) }

  validates :hours, presence: true,
            numericality:     { greater_than: 0, only_integer: true }
  validates :description, presence: true, length: { minimum: 10 }

  def self.next_incentive_for_hours(hours)
    where(arel_table[:hours].gt(hours)).last
  end

  def description_markdown
    to_markdown description
  end
end
