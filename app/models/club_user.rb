# == Schema Information
#
# Table name: club_users
#
#  id                    :integer          not null, primary key
#  club_id               :integer
#  user_id               :integer
#  receive_communication :boolean
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  role                  :integer          default("0")
#

class ClubUser < ApplicationRecord
  enum role: [:member, :leader, :admin, :owner]

  belongs_to :club
  belongs_to :user

  validates :club_id, uniqueness: { scope: :user_id }
  validates :user, presence: true
  validates :club, presence: true
end
