# == Schema Information
#
# Table name: emails
#
#  id         :integer          not null, primary key
#  club_id    :integer
#  subject    :string
#  message    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Email < ApplicationRecord
  include YazdaMarkdown

  belongs_to :club

  validates :subject, presence: true, length: { maximum: 255, minimum: 5 }
  validates :message, presence: true, length: { minimum: 5 }

  def message_markdown
    to_markdown message.to_s
  end
end
