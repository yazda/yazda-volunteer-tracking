# == Schema Information
#
# Table name: volunteers
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  adventure_id           :integer
#  track_time             :boolean          default("false")
#  yazda_user_id          :integer
#  hours                  :decimal(5, 2)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  last_emailed_timestamp :datetime
#  club_id                :integer
#  attended               :boolean          default("false")
#

class Volunteer < ApplicationRecord
  belongs_to :adventure
  belongs_to :user
  belongs_to :club

  delegate :name, :profile_image_url, to: :user

  accepts_nested_attributes_for :user

  scope :trackable, (proc do
    t = Adventure.arel_table

    left_outer_joins(:adventure, :club)
      .where(arel_table[:track_time].eq(true)
               .or(t[:track_time].eq(true)))
  end)

  validates :adventure, presence: true, uniqueness: { scope: :user_id }
  validates :hours,
            presence:     true,
            numericality: { greater_than_or_equal_to: 0 },
            if:           :hours_changed?,
            on:           :update
  validates :yazda_user_id,
            presence:     true,
            numericality: {
              only_integer:             true,
              greater_than_or_equal_to: 1
            },
            if:           :yazda_user_id

  before_validation :set_attended
  after_update :submit_hours_email, if: :track_time_changed?
  after_save :check_incentives, if: :hours_changed?

  private

  def submit_hours_email
    return unless track_time?

    VolunteerMailer.submit_hours(self).deliver_later
    VolunteerMailer.follow_up_submit_hours(self).deliver_later(wait: 1.day)
    VolunteerMailer.follow_up_submit_hours(self).deliver_later(wait: 3.days)
    VolunteerMailer.follow_up_submit_hours(self).deliver_later(wait: 1.week)
  end

  def check_incentives
    total_hours          = user.hours_for_club(club)
    previous_total_hours = total_hours - hours
    incentive            = club.incentives.next_incentive_for_hours previous_total_hours

    return unless incentive.present?

    if incentive.hours > previous_total_hours && total_hours >= incentive.hours
      VolunteerMailer.incentive_achieved(self, incentive).deliver_later
    end
  end

  def set_attended
    return unless hours_changed? && hours.present?

    self.attended = true
  end
end
