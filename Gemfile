source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

source 'https://rails-assets.org' do
  gem 'rails-assets-d3'
  gem 'rails-assets-c3'
  gem 'rails-assets-clipboard'
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.1'
gem 'pg'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'foundation-rails'
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'turbolinks', '~> 5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
gem 'omniauth', '~> 1.3'
gem 'sorcery'
gem 'omniauth-oauth2'
gem 'yazda-api', git: 'https://github.com/jstoup111/yazda-api.git', branch: 'v-0.1.0'
gem 'newrelic_rpm'
gem 'simple_form'
gem 'active_link_to'
gem 'rails_admin'
gem 'kaminari'
gem 'acts-as-taggable-on'
gem 'selectize-rails'
gem 'pundit'
gem 'redcarpet'
gem 'browser-timezone-rails'
gem 'best_in_place'
gem 'responders'
gem 'redis-rails'
gem 'google-tag-manager-rails'
gem 'cocoon'
gem 'migration_data'
gem 'ionicons-rails'
gem 'groupdate'
gem 'friendly_id'
gem 'acts_as_list'
gem 'dry-types'
gem 'dry-struct'
gem 'social-share-button'

gem 'sidekiq'
gem 'sidekiq-failures'
gem 'sidekiq-scheduler'

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'guard'
  gem 'policy-assertions'
  gem 'guard-minitest'
  gem 'guard-brakeman'
  gem 'guard-rubocop'
  gem 'rubocop'
  gem 'mocha'
  gem 'faker'
end

group :test do
  gem 'shoulda'
  gem 'shoulda-matchers'
  gem 'minitest-spec-rails'
  gem 'simplecov', require: false
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'

  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'bullet'
  gem 'lol_dba'
  gem 'peek'
  gem 'flay'
  gem 'annotate'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
