require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Yazda < OmniAuth::Strategies::OAuth2
      option :name, :yazda

      option :client_options,
             site:          'https://api.yazdaapp.com',
             authorize_url: '/oauth/authorize',
             token_url:     '/oauth/token'

      uid { raw_info['user']['id'] }

      info do
        {
          name:                    raw_info['user']['name'],
          email:                   raw_info['user']['email'],
          profile_image_url:       raw_info['user']['profile_image_url'],
          profile_image_thumb_url: raw_info['user']['profile_image_thumb_url'],
          banner_image_url:        raw_info['user']['banner_image_url'],
          banner_image_thumb_url:  raw_info['user']['banner_image_thumb_url'],
          clubs:                   raw_info['user']['clubs'].to_a
          # and anything else you want to return to your API consumers
        }
      end

      def raw_info
        @raw_info ||= access_token.get('/v1/account/me').parsed
      end

      # https://github.com/intridea/omniauth-oauth2/issues/81
      def callback_url
        full_host + script_name + callback_path
      end
    end
  end
end
