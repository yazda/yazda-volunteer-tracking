module Yazda
  module Api
    module VolunteerTracking
      include Common

      # Fetch information about a specific adventure
      #
      # See {http://Yazda.github.io/api/adventures/} for full details
      #
      # @param args any additional arguments
      # @param options (see #get_object)
      # @param block post processing code block
      #
      # @return activity json (see http://Yazda.github.io/api/activities/)
      def volunteer_tracking_adventures(args = {}, options = {}, &block)
        # Fetches the connections for given object.
        api_call('internal/volunteer_trackings/adventures', args, 'get', options, &block)
      end

      def volunteer_tracking_users(args = {}, options = {}, &block)
        # Fetches the connections for given object.
        api_call('internal/volunteer_trackings/users', args, 'get', options, &block)
      end
    end
  end
end

module Yazda
  module Api
    class Client
      include Yazda::Api::VolunteerTracking
    end
  end
end
