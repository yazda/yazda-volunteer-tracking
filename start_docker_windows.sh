#!/usr/bin/env bash

COMPOSE_CONVERT_WINDOWS_PATHS=1 docker-share mount --transient

cd ../yazda-server
COMPOSE_CONVERT_WINDOWS_PATHS=1 docker-share mount --transient
COMPOSE_CONVERT_WINDOWS_PATHS=1 docker-compose build
COMPOSE_CONVERT_WINDOWS_PATHS=1 docker-compose run volunteer rake db:migrate
COMPOSE_CONVERT_WINDOWS_PATHS=1 docker-compose run yazda rake db:migrate

COMPOSE_CONVERT_WINDOWS_PATHS=1 docker-compose up
