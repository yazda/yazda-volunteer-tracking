# README
[![Build Status](https://semaphoreci.com/api/v1/projects/bcb230b4-fcb7-4455-8657-8799f6448caa/1157684/badge.svg)](https://semaphoreci.com/jstoup111/yazda-volunteer-tracking)

# Ruby version

2.3.1

# Configuration

This application needs to be setup under `yazda-server`
 
 ```
 cd ../yazda-server 
 docker-compose run volunteer rake db:setup
 ```

# .ENV

With Docker running go to http://dev.yazdaapp.com/oauth/applications

Edit the third Application
 
 The Redirect URI should be `http://yazda-volunteer-tracking
 .com/auth/yazda/callback`
 
 Copy the Application Key and Secret and paste them respectively into your 
 .env file