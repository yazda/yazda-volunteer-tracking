class RemoveNullOnYazdaUserId < ActiveRecord::Migration[5.0]
  def up
    remove_index :users, :yazda_user_id
    add_index :users, :yazda_user_id

    add_index :volunteers, :yazda_user_id

    change_column :volunteers, :yazda_user_id, :integer, null: true
  end

  def down
    remove_index :users, :yazda_user_id
    add_index :users, :yazda_user_id

    add_index :volunteers, :yazda_user_id, unique: true

    change_column :volunteers, :yazda_user_id, :integer, null: false
  end
end
