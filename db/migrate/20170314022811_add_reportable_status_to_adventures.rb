class AddReportableStatusToAdventures < ActiveRecord::Migration[5.0]
  def change
    add_column :adventures, :reportable_status, :integer, default: 0
  end
end
