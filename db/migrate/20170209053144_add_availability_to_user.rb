class AddAvailabilityToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :monday_morning, :boolean, default: false, null: false
    add_column :users, :monday_afternoon, :boolean, default: false, null: false
    add_column :users, :monday_evening, :boolean, default: false, null: false

    add_column :users, :tuesday_morning, :boolean, default: false, null: false
    add_column :users, :tuesday_afternoon, :boolean, default: false, null: false
    add_column :users, :tuesday_evening, :boolean, default: false, null: false

    add_column :users, :wednesday_morning, :boolean, default: false, null: false
    add_column :users, :wednesday_afternoon, :boolean, default: false, null: false
    add_column :users, :wednesday_evening, :boolean, default: false, null: false

    add_column :users, :thursday_morning, :boolean, default: false, null: false
    add_column :users, :thursday_afternoon, :boolean, default: false, null: false
    add_column :users, :thursday_evening, :boolean, default: false, null: false

    add_column :users, :friday_morning, :boolean, default: false, null: false
    add_column :users, :friday_afternoon, :boolean, default: false, null: false
    add_column :users, :friday_evening, :boolean, default: false, null: false

    add_column :users, :saturday_morning, :boolean, default: false, null: false
    add_column :users, :saturday_afternoon, :boolean, default: false, null: false
    add_column :users, :saturday_evening, :boolean, default: false, null: false

    add_column :users, :sunday_morning, :boolean, default: false, null: false
    add_column :users, :sunday_afternoon, :boolean, default: false, null: false
    add_column :users, :sunday_evening, :boolean, default: false, null: false
  end
end
