class AddClubToVolunteer < ActiveRecord::Migration[5.0]
  def change
    add_reference :volunteers, :club, foreign_key: true
  end

  def data
    Volunteer.find_each do |v|
      v.update_column :club_id, v.adventure.club_id
    end
  end
end
