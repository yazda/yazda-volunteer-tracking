class CreateIncentives < ActiveRecord::Migration[5.0]
  def change
    create_table :incentives do |t|
      t.belongs_to :club, foreign_key: true, index: true
      t.integer :hours
      t.text :description

      t.timestamps
    end
  end
end
