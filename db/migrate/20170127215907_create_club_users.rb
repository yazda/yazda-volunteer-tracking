class CreateClubUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :club_users do |t|
      t.belongs_to :club, foreign_key: true
      t.belongs_to :user, foreign_key: true
      t.boolean :receive_communication, boolean: true

      t.timestamps
    end

    add_index :club_users, [:club_id, :user_id], unique: true
  end
end
