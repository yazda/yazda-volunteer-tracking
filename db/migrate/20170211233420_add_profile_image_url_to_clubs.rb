class AddProfileImageUrlToClubs < ActiveRecord::Migration[5.0]
  def change
    add_column :clubs, :profile_image_url, :string
  end
end
