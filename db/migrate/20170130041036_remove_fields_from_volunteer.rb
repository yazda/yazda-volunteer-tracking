class RemoveFieldsFromVolunteer < ActiveRecord::Migration[5.0]
  def change
    remove_column :volunteers, :name, :string
    remove_column :volunteers, :profile_image_url, :string
    change_column :volunteers, :hours, :decimal, scale: 2, precision: 5
    add_column :volunteers, :last_emailed_timestamp, :datetime
  end
end
