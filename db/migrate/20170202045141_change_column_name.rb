class ChangeColumnName < ActiveRecord::Migration[5.0]
  def change
    add_column :clubs, :submit_hours_email_text, :text
    remove_column :clubs, :submit_hours_email_test, :text
  end
end
