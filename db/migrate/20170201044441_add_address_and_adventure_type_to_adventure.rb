class AddAddressAndAdventureTypeToAdventure < ActiveRecord::Migration[5.0]
  def change
    add_column :adventures, :address, :string
    add_column :adventures, :adventure_type, :string
  end
end
