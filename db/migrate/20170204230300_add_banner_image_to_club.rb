class AddBannerImageToClub < ActiveRecord::Migration[5.0]
  def change
    add_column :clubs, :banner_image_url, :string
    remove_column :adventures, :club_banner_image_url, :string
  end
end
