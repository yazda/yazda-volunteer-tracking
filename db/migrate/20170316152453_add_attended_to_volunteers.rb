class AddAttendedToVolunteers < ActiveRecord::Migration[5.0]
  def change
    add_column :volunteers, :attended, :boolean, default: false
  end

  def data
    Volunteer.where.not(hours: nil).update_all attended: true
  end
end
