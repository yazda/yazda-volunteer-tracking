class CreateVolunteers < ActiveRecord::Migration[5.0]
  def change
    create_table :volunteers do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :adventure, foreign_key: true
      t.boolean :track_time, default: false
      t.string :name, null: false
      t.string :profile_image_url
      t.integer :yazda_user_id, null: false
      t.integer :yazda_invite_id, null: false
      t.decimal :hours

      t.timestamps
    end

    add_index :volunteers, [:yazda_invite_id, :adventure_id], unique: true
    add_index :volunteers, [:yazda_user_id, :hours]
  end
end
