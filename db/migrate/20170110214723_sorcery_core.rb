class SorceryCore < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.integer :yazda_user_id
      t.string :profile_image_url
      t.string :name
      t.string :access_token
      t.string :crypted_password
      t.string :salt

      t.timestamps null: false
    end

    add_index :users, :yazda_user_id, unique: true
  end
end
