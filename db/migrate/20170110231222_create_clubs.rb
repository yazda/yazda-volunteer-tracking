class CreateClubs < ActiveRecord::Migration[5.0]
  def change
    create_table :clubs do |t|
      t.integer :yazda_club_id
      t.string :name
      t.string :website
      t.integer :club_status, default: 0
      t.boolean :volunteers_submit_hours, default: false
      t.boolean :allow_enabled, default: false

      t.timestamps
    end

    add_index :clubs, :yazda_club_id, unique: true
  end
end
