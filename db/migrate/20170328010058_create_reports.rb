class CreateReports < ActiveRecord::Migration[5.0]
  def change
    create_table :reports do |t|
      t.string :name, null: false
      t.string :url, null: false
      t.text :slug, null: false
      t.text :description
      t.string :primary_color
      t.string :secondary_color
      t.date :start_date, null: false
      t.date :end_date
      t.text :tags_list
      t.references :club, foreign_key: true
      t.integer :status, default: 0, index: true

      t.timestamps
    end

    add_index :reports, :slug, unique: true
  end
end
