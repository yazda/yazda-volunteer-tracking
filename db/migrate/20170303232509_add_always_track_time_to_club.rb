class AddAlwaysTrackTimeToClub < ActiveRecord::Migration[5.0]
  def change
    add_column :clubs, :always_track_time, :boolean
  end
end
