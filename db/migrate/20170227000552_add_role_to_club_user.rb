class AddRoleToClubUser < ActiveRecord::Migration[5.0]
  def change
    add_column :club_users, :role, :integer, default: 0
  end
end
