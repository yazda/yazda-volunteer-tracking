class CreateEmails < ActiveRecord::Migration[5.0]
  def change
    create_table :emails do |t|
      t.belongs_to :club, foreign_key: true
      t.string :subject
      t.text :message

      t.timestamps
    end
  end
end
