class CreateAdventures < ActiveRecord::Migration[5.0]
  def change
    create_table :adventures do |t|
      t.integer :yazda_adventure_id
      t.string :name
      t.datetime :start_time
      t.datetime :end_time
      t.string :owner_name
      t.string :owner_profile_image_url
      t.string :club_banner_image_url
      t.boolean :track_time, default: false
      t.belongs_to :club, foreign_key: true, index: true

      t.timestamps
    end

    add_index :adventures, :yazda_adventure_id, unique: true
  end
end
