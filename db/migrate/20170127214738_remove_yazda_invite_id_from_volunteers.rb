class RemoveYazdaInviteIdFromVolunteers < ActiveRecord::Migration[5.0]
  def change
    remove_index :volunteers, [:yazda_invite_id, :adventure_id]
    remove_column :volunteers, :yazda_invite_id, :integer, null: false

    add_index :volunteers, [:user_id, :adventure_id], unique: true
  end
end
