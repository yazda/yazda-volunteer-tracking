class CreateReportItems < ActiveRecord::Migration[5.0]
  def change
    create_table :report_items do |t|
      t.references :report, foreign_key: true
      t.text :content
      t.integer :item_type
      t.integer :position
      t.jsonb :options, default: '{}'

      t.timestamps
    end
  end
end
