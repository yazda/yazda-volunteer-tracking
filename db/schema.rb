# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170407133249) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "adventures", force: :cascade do |t|
    t.integer  "yazda_adventure_id"
    t.string   "name"
    t.datetime "start_time"
    t.datetime "end_time"
    t.string   "owner_name"
    t.string   "owner_profile_image_url"
    t.boolean  "track_time",              default: false
    t.integer  "club_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "address"
    t.string   "adventure_type"
    t.integer  "reportable_status",       default: 0
    t.index ["club_id"], name: "index_adventures_on_club_id", using: :btree
    t.index ["yazda_adventure_id"], name: "index_adventures_on_yazda_adventure_id", unique: true, using: :btree
  end

  create_table "club_users", force: :cascade do |t|
    t.integer  "club_id"
    t.integer  "user_id"
    t.boolean  "receive_communication"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "role",                  default: 0
    t.index ["club_id", "user_id"], name: "index_club_users_on_club_id_and_user_id", unique: true, using: :btree
    t.index ["club_id"], name: "index_club_users_on_club_id", using: :btree
    t.index ["user_id"], name: "index_club_users_on_user_id", using: :btree
  end

  create_table "clubs", force: :cascade do |t|
    t.integer  "yazda_club_id"
    t.string   "name"
    t.string   "website"
    t.integer  "club_status",             default: 0
    t.boolean  "volunteers_submit_hours", default: false
    t.boolean  "allow_enabled",           default: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.text     "submit_hours_email_text"
    t.string   "banner_image_url"
    t.string   "profile_image_url"
    t.boolean  "always_track_time"
    t.index ["yazda_club_id"], name: "index_clubs_on_yazda_club_id", unique: true, using: :btree
  end

  create_table "emails", force: :cascade do |t|
    t.integer  "club_id"
    t.string   "subject"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["club_id"], name: "index_emails_on_club_id", using: :btree
  end

  create_table "incentives", force: :cascade do |t|
    t.integer  "club_id"
    t.integer  "hours"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["club_id"], name: "index_incentives_on_club_id", using: :btree
  end

  create_table "report_items", force: :cascade do |t|
    t.integer  "report_id"
    t.text     "content"
    t.integer  "item_type"
    t.integer  "position"
    t.jsonb    "options",    default: "{}"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["report_id"], name: "index_report_items_on_report_id", using: :btree
  end

  create_table "reports", force: :cascade do |t|
    t.string   "name",                        null: false
    t.string   "url",                         null: false
    t.text     "slug",                        null: false
    t.text     "description"
    t.string   "primary_color"
    t.string   "secondary_color"
    t.date     "start_date",                  null: false
    t.date     "end_date"
    t.text     "tags_list"
    t.integer  "club_id"
    t.integer  "status",          default: 0
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["club_id"], name: "index_reports_on_club_id", using: :btree
    t.index ["slug"], name: "index_reports_on_slug", unique: true, using: :btree
    t.index ["status"], name: "index_reports_on_status", using: :btree
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.string   "taggable_type"
    t.integer  "taggable_id"
    t.string   "tagger_type"
    t.integer  "tagger_id"
    t.string   "context",       limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context", using: :btree
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
    t.index ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy", using: :btree
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id", using: :btree
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type", using: :btree
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type", using: :btree
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true, using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.integer  "yazda_user_id"
    t.string   "profile_image_url"
    t.string   "name"
    t.string   "access_token"
    t.string   "crypted_password"
    t.string   "salt"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email"
    t.boolean  "monday_morning",      default: false, null: false
    t.boolean  "monday_afternoon",    default: false, null: false
    t.boolean  "monday_evening",      default: false, null: false
    t.boolean  "tuesday_morning",     default: false, null: false
    t.boolean  "tuesday_afternoon",   default: false, null: false
    t.boolean  "tuesday_evening",     default: false, null: false
    t.boolean  "wednesday_morning",   default: false, null: false
    t.boolean  "wednesday_afternoon", default: false, null: false
    t.boolean  "wednesday_evening",   default: false, null: false
    t.boolean  "thursday_morning",    default: false, null: false
    t.boolean  "thursday_afternoon",  default: false, null: false
    t.boolean  "thursday_evening",    default: false, null: false
    t.boolean  "friday_morning",      default: false, null: false
    t.boolean  "friday_afternoon",    default: false, null: false
    t.boolean  "friday_evening",      default: false, null: false
    t.boolean  "saturday_morning",    default: false, null: false
    t.boolean  "saturday_afternoon",  default: false, null: false
    t.boolean  "saturday_evening",    default: false, null: false
    t.boolean  "sunday_morning",      default: false, null: false
    t.boolean  "sunday_afternoon",    default: false, null: false
    t.boolean  "sunday_evening",      default: false, null: false
    t.index ["yazda_user_id"], name: "index_users_on_yazda_user_id", using: :btree
  end

  create_table "volunteers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "adventure_id"
    t.boolean  "track_time",                                     default: false
    t.integer  "yazda_user_id"
    t.decimal  "hours",                  precision: 5, scale: 2
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.datetime "last_emailed_timestamp"
    t.integer  "club_id"
    t.boolean  "attended",                                       default: false
    t.index ["adventure_id"], name: "index_volunteers_on_adventure_id", using: :btree
    t.index ["club_id"], name: "index_volunteers_on_club_id", using: :btree
    t.index ["user_id", "adventure_id"], name: "index_volunteers_on_user_id_and_adventure_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_volunteers_on_user_id", using: :btree
    t.index ["yazda_user_id", "hours"], name: "index_volunteers_on_yazda_user_id_and_hours", using: :btree
    t.index ["yazda_user_id"], name: "index_volunteers_on_yazda_user_id", using: :btree
  end

  add_foreign_key "adventures", "clubs"
  add_foreign_key "club_users", "clubs"
  add_foreign_key "club_users", "users"
  add_foreign_key "emails", "clubs"
  add_foreign_key "incentives", "clubs"
  add_foreign_key "report_items", "reports"
  add_foreign_key "reports", "clubs"
  add_foreign_key "volunteers", "adventures"
  add_foreign_key "volunteers", "clubs"
  add_foreign_key "volunteers", "users"
end
