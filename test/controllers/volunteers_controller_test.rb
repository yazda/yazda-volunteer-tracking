require 'test_helper'

class VolunteersControllerTest < ActionController::TestCase
  describe 'routing' do
    it 'has correct routes' do
      assert_routing '/volunteers', controller: 'volunteers', action: 'index'
      assert_routing '/volunteers/new', controller: 'volunteers', action: 'new'
      assert_routing({ path: '/volunteers', method: 'post' },
                     controller: 'volunteers', action: 'create')
      assert_routing({ path: '/volunteers/1', method: 'patch' },
                     controller: 'volunteers', action: 'update', id: '1')
    end
  end

  describe 'not logged in' do
    describe '#index' do
      it 'redirects to root' do
        get :index
        assert_redirected_to :root
      end
    end

    describe '#new' do
      it 'redirects to root' do
        get :new
        assert_redirected_to :root
      end
    end

    describe '#create' do
      it 'redirects to root' do
        post :create, params: { id: 1 }
        assert_redirected_to :root
      end
    end

    describe '#update' do
      it 'redirects to root' do
        patch :update, params: { id: 1 }
        assert_redirected_to :root
      end
    end
  end

  # test "should get update" do
  #   get volunteers_update_url
  #   assert_response :success
  # end
  # TODO:
end
