require 'test_helper'

class AccountsControllerTest < ActionDispatch::IntegrationTest
  def test_routes
    assert_routing '/account', controller: 'accounts', action: 'show'
    assert_routing({ path: '/account', method: 'put' },
                   controller: 'accounts', action: 'update')
  end

  describe 'not logged in' do
    describe '#show' do
      it 'redirects to root' do
        get account_path
        assert_redirected_to :root
      end
    end

    describe '#update' do
      it 'redirects to root' do
        patch account_path, params: { my: 'params' }
        assert_redirected_to :root
      end
    end
  end

  describe 'logged in' do
    before do
      user = users(:one)
      login_user user
    end

    describe '#show' do
      it 'redirects to root' do
        get account_path
        assert_response :ok
      end

      # TODO: test that the elements should be in the DOM
    end
  end
end
