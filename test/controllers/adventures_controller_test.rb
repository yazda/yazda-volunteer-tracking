require 'test_helper'

class AdventuresControllerTest < ActionDispatch::IntegrationTest
  describe 'routing' do
    it 'has correct routes' do
      assert_routing '/adventures', controller: 'adventures', action: 'index'
      assert_routing '/adventures/1/edit',
                     controller: 'adventures',
                     action:     'edit', id: '1'
      assert_routing({ path: '/adventures/1', method: 'patch' },
                     controller: 'adventures', action: 'update', id: '1')
      assert_routing({ path: '/adventures/1/track_time', method: 'post' },
                     controller: 'adventures', action: 'track_time', id: '1')
      assert_routing({ path:   '/adventures/1/set_time_for_volunteers',
                       method: 'post' },
                     controller: 'adventures',
                     action:     'set_time_for_volunteers',
                     id:         '1')
    end
  end

  describe 'not logged in' do
    describe '#index' do
      it 'redirects to root' do
        get adventures_path
        assert_redirected_to :root
      end
    end

    describe '#set_time_for_volunteers_adventure_path' do
      it 'redirects to root' do
        post set_time_for_volunteers_adventure_path(1)
        assert_redirected_to :root
      end
    end

    describe '#track_time_adventure_path' do
      it 'redirects to root' do
        post track_time_adventure_path(1)
        assert_redirected_to :root
      end
    end

    describe '#edit' do
      it 'redirects to root' do
        get edit_adventure_path 1
        assert_redirected_to :root
      end
    end

    describe '#update' do
      it 'redirects to root' do
        patch adventure_path 1
        assert_redirected_to :root
      end
    end
  end

  describe 'logged in' do
    before do
      @user = users(:one)
      clubs = Club.all.map do |c|
        { 'id' => c.yazda_club_id, 'role' => 'admin', 'name' => c.name }
      end

      login_user @user, clubs
    end

    describe '#index' do
      it 'is successful' do
        get adventures_path
        assert_response :success
      end

      # TODO: test correct values are in dom
      # TODO: test sorting
      # TODO: test filtering
    end

    describe '#edit' do
      before do
        @adventure = Adventure.first
        Adventure.stubs(:find).returns(@adventure)
      end

      it 'is successful' do
        get edit_adventure_path(1)
        assert_response :success
      end

      # TODO: test correct values are in dom
    end

    # [:track_time, :set_time_for_volunteers].each do |method|
    #   describe "##{method}" do
    #     it 'redirects to root' do
    #       post method, params: { id: 1 }
    #       assert_redirected_to :root
    #     end
    #   end
    # end
    #
    # describe '#update' do
    #   it 'redirects to root' do
    #     patch :update, params: { id: 1 }
    #     assert_redirected_to :root
    #   end
    # end
  end
end
