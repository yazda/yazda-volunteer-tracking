require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  def test_routes
    assert_routing '/', controller: 'home', action: 'index'
  end
end
