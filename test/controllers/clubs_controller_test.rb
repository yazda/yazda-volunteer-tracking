require 'test_helper'

class ClubsControllerTest < ActionController::TestCase
  describe 'routing' do
    it 'has correct routes' do
      assert_routing '/clubs/1/edit',
                     controller: 'clubs',
                     action:     'edit', id: '1'
      assert_routing({ path: '/clubs/1', method: 'patch' },
                     controller: 'clubs', action: 'update', id: '1')
      assert_routing({ path: '/clubs/1/enable', method: 'patch' },
                     controller: 'clubs', action: 'enable', id: '1')
      assert_routing({ path: '/clubs/1/disable', method: 'patch' },
                     controller: 'clubs', action: 'disable', id: '1')
    end
  end

  describe 'not logged in' do
    describe '#edit' do
      it 'redirects to root' do
        get :edit, params: { id: 1 }
        assert_redirected_to :root
      end
    end

    [:enable, :disable, :update].each do |method|
      describe "##{method}" do
        it 'redirects to root' do
          post method, params: { id: 1 }
          assert_redirected_to :root
        end
      end
    end
  end

  # describe 'logged in' do
  #   before do
  #     @user                       = users(:one)
  #     @controller.session[:clubs] = Club.all.map do |c|
  #       { 'id' => c.yazda_club_id, 'role' => 'admin', 'name' => c.name }
  #     end
  #     login_user @user
  #   end
  #
  #   describe '#index' do
  #     it 'is successful' do
  #       get :index
  #       assert_response :success
  #     end
  #
  #     # TODO: test this
  #     # it 'assigns adventures' do
  #     #   get :index
  #     #   assert_equal [adventures(:one)], assigns(:adventures)
  #     # end
  #
  #     # TODO: test sorting
  #   end
  #
  #   describe '#edit' do
  #     before do
  #       @adventure = Adventure.first
  #       Adventure.stubs(:find).returns(@adventure)
  #     end
  #
  #     it 'is successful' do
  #       get :edit, params: { id: 1 }
  #       assert_response :success
  #     end
  #
  #     it 'assigns adventure' do
  #       get :edit, params: { id: 1 }
  #       assert_equal @adventure, assigns(:adventure)
  #     end
  #   end
  #
  #   # [:track_time, :set_time_for_volunteers].each do |method|
  #   #   describe "##{method}" do
  #   #     it 'redirects to root' do
  #   #       post method, params: { id: 1 }
  #   #       assert_redirected_to :root
  #   #     end
  #   #   end
  #   # end
  #   #
  #   # describe '#update' do
  #   #   it 'redirects to root' do
  #   #     patch :update, params: { id: 1 }
  #   #     assert_redirected_to :root
  #   #   end
  #   # end
  # end
end
