require 'test_helper'

class SessionClubServiceTest < ActiveSupport::TestCase
  before do
    @user    = User.first
    @service = SessionClubService.new(@user)
  end

  describe '#enabled_clubs' do
    it 'returns enabled clubs' do
      ids      = Club.pluck :yazda_club_id
      statuses = @service.enabled_clubs(ids.to_a).map(&:club_status)

      statuses.each do |s|
        s.must_equal 'enabled'
      end
    end
  end

  describe '#selected_club' do
    before { @session = {} }

    it 'gets the users first enabled club' do
      club                = Club.first
      @user.enabled_clubs = Club.all

      @service.selected_club(@session).must_equal club
      @session[:selected_club_id].must_equal club.id
    end

    it 'gets the selected club stored in session' do
      club                        = Club.last
      @user.enabled_clubs         = Club.all
      @session[:selected_club_id] = club.id

      @service.selected_club(@session).must_equal club
      @session[:selected_club_id].must_equal club.id
    end

    it 'returns nil' do
      @user.enabled_clubs = Club.none

      @service.selected_club(@session).must_be_nil
      @session[:selected_club_id].must_be_nil
    end

    it 'returns a found club' do
      club                        = Club.last
      @user.enabled_clubs         = Club.all
      @session[:selected_club_id] = club.id

      @service.selected_club(@session, club).must_equal club
      @session[:selected_club_id].must_equal club.id
    end
  end
end
