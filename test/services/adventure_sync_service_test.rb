require 'test_helper'

class AdventureUserServiceTest < ActiveSupport::TestCase
  # setup do
  #   @user   = User.new
  #   @client = Minitest::Mock.new
  #
  #   @user.expects(:access_token).returns 'test'
  #   @user.expects(:selected_club).returns(Struct.new(:id, :yazda_club_id)
  #                                           .new(1, 1))
  #   @user.expects(:selected_club).returns(Struct.new(:id, :yazda_club_id)
  #                                           .new(1, 1))
  #
  #   @subject = AdventureSyncService.new(@user)
  # end
  #
  # # TODO: rewrite all these tests
  #
  # test 'gets adventures and then creates them if they do not exist' do
  #   adventure_class = MiniTest::Mock.new
  #
  #   owner      = { 'name' => 'Test', 'profile_image_url' => 'image' }
  #   club       = { 'banner_image_url' => 'banner_image', 'id' => 1 }
  #   adventures = { 'adventures' => [
  #     {
  #       'id'         => 1,
  #       'name'       => 'name',
  #       'start_time' => 'start_time',
  #       'end_time'   => 'end_time',
  #       'owner'      => owner,
  #       'club'       => club,
  #       'attendings' => [{ 'user' => { 'name'              => 'test',
  #                                      'profile_image_url' => 'profile_url',
  #                                      'id'                => 1 } }]
  #     },
  #     {
  #       'id'         => 2,
  #       'name'       => 'name2',
  #       'start_time' => 'start_time2',
  #       'end_time'   => 'end_time2',
  #       'owner'      => owner,
  #       'club'       => club,
  #       'attendings' => []
  #     }
  #   ] }
  #
  #   @client.expect :adventures, adventures, [{ club_id: 1, status: :past }]
  #
  #   @subject.stub :client, @client do
  #     Adventure.stub :find_by_yazda_adventure_id, false do
  #       Adventure.stub :create, Adventure.new do
  #         Volunteer.stub :create, Volunteer.new do
  #           Club.stub :find, Struct.new(:enabled?).new(true) do
  #             @subject.sync_club_adventures
  #           end
  #         end
  #       end
  #     end
  #   end
  #
  #   # TODO: make sure create calls happened
  #
  #   adventure_class.verify
  # end
  #
  # # test 'getting adventures handles error' do
  # #   exception = ->() { raise(Yazda::Api::ServerError.new(500, 'test')) }
  # #
  # #   @client = Yazda::Api::Client.new
  # #
  # #   @client.stub :adventures, exception do
  # #     @subject.stub :client, @client do
  # #       Club.stub :find, Struct.new(:enabled?).new(true) do
  # #         assert_equal 'There was a problem syncing your adventures',
  # #                      @subject.sync_club_adventures
  # #       end
  # #     end
  # #   end
  # # end
end
