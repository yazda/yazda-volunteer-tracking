require 'minitest/mock'
require 'test_helper'

class YazdaUserServiceTest < ActiveSupport::TestCase
  # TODO: test this
  # Credentials = Struct.new(:token)
  # Info        = Struct.new(:profile_image_url, :name, :clubs, :email)
  # AuthHash    = Struct.new(:credentials, :info, :uid)
  #
  # setup do
  #   @controller = Minitest::Mock.new
  #   @user       = Minitest::Mock.new
  #   @subject    = YazdaUserService.new(@controller, @user)
  # end
  #
  # # TODO: this test isn't working
  # test 'sets user calls save, sets session, creates club, updates Volunteer' do
  #   session      = {}
  #   clubs        = [{ 'id' => 1, 'role' => 'admin', 'name' => 'test' },
  #                   { 'id' => 2, 'role' => 'leader', 'name' => 'test 1' },
  #                   { 'id' => 3, 'role' => 'member', 'name' => 'test 2' }]
  #   symbol_clubs = clubs.map do |club|
  #     club.each_with_object({}) do |(k, v), memo|
  #       memo[k.to_sym] = v
  #       memo
  #     end
  #   end
  #
  #   auth_hash = AuthHash.new(Credentials.new('test'),
  #                            Info.new('image_url', 'name', clubs, 'test@test.com'),
  #                            'uid')
  #
  #   @controller.expect :reset_session, true
  #
  #   @user.expect :save, true
  #   @user.expect :access_token=, nil, [auth_hash.credentials.token]
  #   @user.expect :email=, nil, [auth_hash.info.email]
  #   @user.expect :profile_image_url=, nil, [auth_hash.info.profile_image_url]
  #   @user.expect :name=, nil, [auth_hash.info.name]
  #   @user.expect :yazda_user_id, 99_999
  #   @user.expect :id, 1
  #
  #   Volunteer.stub :where, Volunteer do
  #     Volunteer.stub :update_all, 3 do
  #       assert_equal 3, @subject.save_user(session, auth_hash)
  #     end
  #   end
  #
  #   assert_equal auth_hash.uid, session[:yazda_user_id]
  #   assert_equal symbol_clubs, session[:clubs]
  #
  #   @controller.verify
  #   @user.verify
  # end
  #
  # test 'returns false and does not set session' do
  #   session      = {}
  #   clubs        = [{ 'id' => 1, 'role' => 'admin', 'name' => 'test' },
  #                   { 'id' => 2, 'role' => 'leader', 'name' => 'test 1' },
  #                   { 'id' => 3, 'role' => 'member', 'name' => 'test 2' }]
  #   symbol_clubs = clubs.map do |club|
  #     club.each_with_object({}) do |(k, v), memo|
  #       memo[k.to_sym] = v
  #       memo
  #     end
  #   end
  #
  #   auth_hash = AuthHash.new(Credentials.new('test'),
  #                            Info.new('image_url', 'name', clubs, 'test@test.com'),
  #                            'uid')
  #
  #   @user.expect :save, false
  #   @user.expect :access_token=, nil, [auth_hash.credentials.token]
  #   @user.expect :email=, nil, [auth_hash.info.email]
  #   @user.expect :profile_image_url=, nil, [auth_hash.info.profile_image_url]
  #   @user.expect :name=, nil, [auth_hash.info.name]
  #
  #   assert_equal false, @subject.save_user(session, auth_hash)
  #   refute_equal auth_hash.uid, session[:yazda_user_id]
  #   refute_equal symbol_clubs, session[:clubs]
  #
  #   @controller.verify
  #   @user.verify
  # end
end
