require 'test_helper'

class VolunteerPolicyTest < ActiveSupport::TestCase
  describe '#index?' do
    before { @user = users(:one) }

    it 'returns true if selected_club' do
      @user.selected_club = Club.first

      assert_permit @user, Volunteer, :index
    end

    it 'returns false if not selected_club' do
      refute_permit @user, Volunteer, :index
    end
  end

  describe '#update?' do
    before do
      @user           = users(:one)
      @volunteer      = volunteers(:one)
      @volunteer.user = @user
      @volunteer.club = clubs(:one)
    end

    it 'returns true if selected_club' do
      @user.selected_club = Club.first

      assert_permit @user, @volunteer, :update
    end

    it 'returns true if same user' do
      @user.selected_club = nil

      assert_permit @user, @volunteer, :update
    end

    it 'returns false in if not same user and not selected club' do
      @user.selected_club = nil
      @volunteer.user = users(:two)

      refute_permit @user, @volunteer, :update
    end
  end

  describe '#create?' do
    before do
      @user           = users(:one)
      @volunteer      = volunteers(:one)
      @volunteer.user = @user
      @volunteer.club = clubs(:one)
    end

    it 'returns true if selected_club' do
      @user.selected_club = Club.first

      assert_permit @user, @volunteer, :create
    end

    it 'returns true if same user' do
      @user.selected_club = nil

      assert_permit @user, @volunteer, :create
    end

    it 'returns false in if not same user and not selected club' do
      @user.selected_club = nil
      @volunteer.user = users(:two)

      refute_permit @user, @volunteer, :create
    end
  end
end
