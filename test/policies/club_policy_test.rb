require 'test_helper'

class ClubPolicyTest < ActiveSupport::TestCase
  describe '#edit?' do
    before do
      @user    = users(:one)
      @club    = clubs(:one)
      @club.id = 1
    end

    [:edit, :update, :enable, :disable].each do |method|
      it "#{method} returns true if admin_clubs" do
        @user.imported_clubs = [{ 'id' => 1, 'role' => 'admin' }]

        assert_permit @user, @club, method
      end

      it "#{method} returns false if not admin_clubs" do
        @user.imported_clubs = [{ 'id' => 1, 'role' => 'leader' }]

        refute_permit @user, @club, method
      end

      it "#{method} returns false if club not included" do
        @user.imported_clubs = [{ 'id' => 4, 'role' => 'leader' }]

        refute_permit @user, @club, method
      end
    end
  end
end
