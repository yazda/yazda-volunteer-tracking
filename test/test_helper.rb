ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/spec'
require 'mocha/mini_test'
require 'simplecov'
require 'sidekiq/testing'

# SimpleCov.minimum_coverage 90
SimpleCov.start 'rails' do
  add_group 'Services', 'app/services'
end

module SidekiqMinitestSupport
  def after_teardown
    Sidekiq::Worker.clear_all
    super
  end
end

class MiniTest::Spec
  include SidekiqMinitestSupport
end

module ActiveSupport
  class TestCase
    include SidekiqMinitestSupport

    # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
    fixtures :all

    # Add more helper methods to be used by all tests here...
    def assert_permit(user, record, action)
      msg = "User #{user.inspect} should be permitted to #{action} #{record}, but isn't permitted"
      assert permit(user, record, action), msg
    end

    def refute_permit(user, record, action)
      msg = "User #{user.inspect} should NOT be permitted to #{action} #{record}, but is permitted"
      refute permit(user, record, action), msg
    end

    def permit(user, record, action)
      cls = self.class.superclass.to_s.gsub(/Test/, '')
      cls.constantize.new(user, record).public_send("#{action.to_s}?")
    end
  end
end

module ActionDispatch
  class IntegrationTest
    def setup_controller(user, clubs)
      OmniAuth.config.test_mode = true

      auth_hash = {
        provider:    'yazda',
        uid:         user.yazda_user_id,
        credentials: {
          token: 'test'
        },
        info:        {
          name:              user.name,
          email:             user.email,
          profile_image_url: user.profile_image_url,
          clubs:             clubs
        }
      }

      OmniAuth.config.mock_auth[:default] = OmniAuth::AuthHash.new(auth_hash)
    end

    def login_user(user = nil, clubs = [])
      user ||= @user
      setup_controller(user, clubs)

      # YazdaUserService.any_instance.stubs(:save_user).returns(true)
      get '/auth/yazda/callback'
    end

    # Accepts route and HTTP method arguments
    # Default - 'logout_url' and GET
    def logout_user(route = nil, http_method = :get)
      route ||= logout_url
      page.driver.send(http_method, route)
    end
  end
end
