# == Schema Information
#
# Table name: volunteers
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  adventure_id           :integer
#  track_time             :boolean          default("false")
#  yazda_user_id          :integer
#  hours                  :decimal(5, 2)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  last_emailed_timestamp :datetime
#  club_id                :integer
#  attended               :boolean          default("false")
#

require 'test_helper'

class VolunteerTest < ActiveSupport::TestCase
  # TODO: test this
  # should validate_presence_of(:yazda_user_id)
  # should(validate_numericality_of(:yazda_user_id)
  #          .only_integer
  #          .is_greater_than_or_equal_to(1))

  should validate_presence_of(:adventure)

  # should validate_presence_of(:hours).on(:update) TODO: on
  # should validate_numericality_of(:hours).is_greater_than(0).on(:update) TODO: on

  should belong_to(:adventure)
  should belong_to(:user)
  should belong_to(:club)

  describe '#trackable scope' do
    it 'includes when .track_time is true' do
      Volunteer.trackable.must_include volunteers(:trackable)
    end

    it 'includes when adventure .track_time is true' do
      Volunteer.trackable
        .must_include volunteers(:adventure_trackable_volunteer_not)
      Volunteer.trackable.must_include volunteers(:adventure_trackable)
    end

    it 'does not include when .track_time is false for adventure & volunteer' do
      Volunteer.trackable
        .wont_include(volunteers(:adventure_not_trackable_user_not_trackable))
    end
  end
end
