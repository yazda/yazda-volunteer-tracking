# == Schema Information
#
# Table name: clubs
#
#  id                      :integer          not null, primary key
#  yazda_club_id           :integer
#  name                    :string
#  website                 :string
#  club_status             :integer          default("0")
#  volunteers_submit_hours :boolean          default("false")
#  allow_enabled           :boolean          default("false")
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  submit_hours_email_text :text
#  banner_image_url        :string
#  profile_image_url       :string
#  always_track_time       :boolean
#

require 'test_helper'

class ClubTest < ActiveSupport::TestCase
  should validate_presence_of(:yazda_club_id)
  should validate_uniqueness_of(:yazda_club_id)
  should(validate_numericality_of(:yazda_club_id)
           .only_integer
           .is_greater_than_or_equal_to(1))

  should validate_presence_of(:name)
  should validate_length_of(:name).is_at_most(255)

  should validate_length_of(:website).is_at_most(255)

  should define_enum_for(:club_status).with([:disabled, :enabled])

  should have_many(:adventures).dependent(:destroy)
  should have_many(:volunteers).through(:adventures)
  should have_many(:incentives).dependent(:destroy)
  should have_many(:reports).dependent(:destroy)

  # TODO: validate tags
  describe 'custom validations' do
    before do
      @club = Club.new(submit_hours_email_text: 'here is a test descriptikon',
                       name:                    'Test club',
                       yazda_club_id:           6)
    end

    it 'only allows 5 incentives' do
      @club.incentives << Incentive.new(hours: 2, description: 'here is a description')
      @club.incentives << Incentive.new(hours: 2, description: 'here is a description')
      @club.incentives << Incentive.new(hours: 2, description: 'here is a description')
      @club.incentives << Incentive.new(hours: 2, description: 'here is a description')
      @club.incentives << Incentive.new(hours: 2, description: 'here is a description')
      @club.incentives << Incentive.new(hours: 2, description: 'here is a description')

      @club.valid?.must_equal false
      @club.errors.messages[:incentives].must_include 'Maximum 5 Incentives'
    end
  end

  describe 'methods' do
    before do
      @club = Club.new(submit_hours_email_text: 'here is a test descriptikon',
                       name:                    'Test club',
                       yazda_club_id:           6)
    end

    describe '#submit_hours_email_markdown' do
      it 'should call #to_markdown' do
        Club.any_instance.expects(:to_markdown).with(@club.submit_hours_email_text)
        @club.submit_hours_email_markdown
      end
    end

    describe '#max_incentive' do
      it 'returns nil' do
        @club.max_incentive.must_be_nil
      end

      it 'returns incentive with max hours' do
        max = Incentive.new(hours: 5, description: 'here is a description')

        @club.incentives << Incentive.new(hours: 2, description: 'here is a description')
        @club.incentives << max
        @club.incentives << Incentive.new(hours: 4, description: 'here is a description')
        @club.save!
        @club.reload

        @club.max_incentive.hours.must_equal 5
      end
    end

    describe '#context_list_underscore' do
      # TODO: test this
    end
  end
end
