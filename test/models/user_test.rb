# == Schema Information
#
# Table name: users
#
#  id                  :integer          not null, primary key
#  yazda_user_id       :integer
#  profile_image_url   :string
#  name                :string
#  access_token        :string
#  crypted_password    :string
#  salt                :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  email               :string
#  monday_morning      :boolean          default("false"), not null
#  monday_afternoon    :boolean          default("false"), not null
#  monday_evening      :boolean          default("false"), not null
#  tuesday_morning     :boolean          default("false"), not null
#  tuesday_afternoon   :boolean          default("false"), not null
#  tuesday_evening     :boolean          default("false"), not null
#  wednesday_morning   :boolean          default("false"), not null
#  wednesday_afternoon :boolean          default("false"), not null
#  wednesday_evening   :boolean          default("false"), not null
#  thursday_morning    :boolean          default("false"), not null
#  thursday_afternoon  :boolean          default("false"), not null
#  thursday_evening    :boolean          default("false"), not null
#  friday_morning      :boolean          default("false"), not null
#  friday_afternoon    :boolean          default("false"), not null
#  friday_evening      :boolean          default("false"), not null
#  saturday_morning    :boolean          default("false"), not null
#  saturday_afternoon  :boolean          default("false"), not null
#  saturday_evening    :boolean          default("false"), not null
#  sunday_morning      :boolean          default("false"), not null
#  sunday_afternoon    :boolean          default("false"), not null
#  sunday_evening      :boolean          default("false"), not null
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  should validate_presence_of(:name)
  should validate_length_of(:name).is_at_most(255)

  should validate_length_of(:access_token).is_at_most(255)

  should validate_presence_of(:email)
  should validate_uniqueness_of(:email).case_insensitive
  # TODO: format of email

  # TODO: test this
  # should validate_presence_of(:yazda_user_id)
  # should validate_uniqueness_of(:yazda_user_id)
  # should validate_numericality_of(:yazda_user_id)
  #          .only_integer
  #          .is_greater_than_or_equal_to(1)

  should have_many(:volunteers)
  should have_many(:unfilled_volunteers)
           .class_name('Volunteer')
  # .conditions(Volunteer.trackable) TODO: how to test conditions

  describe 'scopes' do
    # TODO: test this
    # describe '#filter' do
    #   it 'filters Users' do
    #     user = User.first
    #     user.skill_list << 'hello'
    #     user.save
    #
    #     @users = User.filter('hello')
    #
    #     @users.count.must_equal 1
    #     @users.each do |u|
    #       u.skill_list.must_include('hello')
    #     end
    #   end
    #
    #   it 'returns all' do
    #     @users = User.filter('')
    #
    #     @users.count.must_equal 2
    #   end
    # end

    describe '#search' do
      it 'searches for Users' do
        @users = User.search('test 2')

        @users.count.must_equal 1
        @users.each do |u|
          u.name.must_equal 'test 2'
        end
      end

      it 'returns all' do
        @users = User.search('')

        @users.count.must_equal 2
      end
    end
  end

  describe 'methods' do
    before { @user = User.new }

    describe '#admin_clubs' do
      it 'returns nil if imported_clubs is nil' do
        @user.admin_clubs.must_be_nil
      end

      it 'returns admin clubs' do
        @user.imported_clubs = [{ 'id' => '1', 'role' => 'admin' },
                                { 'id' => '2', 'role' => 'leader' },
                                { 'id' => '3', 'role' => 'member' }]

        @user.admin_clubs.must_include('id' => '1', 'role' => 'admin')
        @user.admin_clubs.wont_include('id' => '2', 'role' => 'leader')
        @user.admin_clubs.wont_include('id' => '3', 'role' => 'member')
      end
    end

    describe '#leader_clubs' do
      it 'returns nil if imported_clubs is nil' do
        @user.leader_clubs.must_be_nil
      end

      it 'returns admin clubs' do
        @user.imported_clubs = [{ 'id' => '1', 'role' => 'admin' },
                                { 'id' => '2', 'role' => 'leader' },
                                { 'id' => '3', 'role' => 'member' }]

        @user.leader_clubs.must_include('id' => '1', 'role' => 'admin')
        @user.leader_clubs.must_include('id' => '2', 'role' => 'leader')
        @user.leader_clubs.wont_include('id' => '3', 'role' => 'member')
      end
    end
  end
end
