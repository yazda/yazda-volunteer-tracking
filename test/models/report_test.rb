# == Schema Information
#
# Table name: clubs
#
#  id                      :integer          not null, primary key
#  yazda_club_id           :integer
#  name                    :string
#  website                 :string
#  club_status             :integer          default("0")
#  volunteers_submit_hours :boolean          default("false")
#  allow_enabled           :boolean          default("false")
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  submit_hours_email_text :text
#  banner_image_url        :string
#  profile_image_url       :string
#  always_track_time       :boolean
#

require 'test_helper'

class ReportTest < ActiveSupport::TestCase
  should validate_presence_of(:name)
  should validate_length_of(:name).is_at_least(3).is_at_most(255)

  should validate_presence_of(:url)
  should validate_length_of(:url).is_at_least(3).is_at_most(255)

  # TODO: validate primary & secondary color

  should validate_presence_of(:start_date)

  should validate_presence_of(:club_id)

  should define_enum_for(:status).with([:draft, :published, :archived])

  should belong_to(:club)

  # describe 'methods' do
  #   before do
  #     @club = Club.new(submit_hours_email_text: 'here is a test descriptikon',
  #                      name:                    'Test club',
  #                      yazda_club_id:           6)
  #   end
  #
  #   describe '#hours' do
  #     it 'should call #to_markdown' do
  #       Club.any_instance.expects(:to_markdown).with(@club.submit_hours_email_text)
  #       @club.submit_hours_email_markdown
  #     end
  #   end
  #
  #   describe '#volunteers' do
  #     it 'returns nil' do
  #       @club.max_incentive.must_be_nil
  #     end
  #
  #     it 'returns incentive with max hours' do
  #       max = Incentive.new(hours: 5, description: 'here is a description')
  #
  #       @club.incentives << Incentive.new(hours: 2, description: 'here is a description')
  #       @club.incentives << max
  #       @club.incentives << Incentive.new(hours: 4, description: 'here is a description')
  #       @club.save!
  #       @club.reload
  #
  #       @club.max_incentive.hours.must_equal 5
  #     end
  #   end
  #
  #   describe '#adventures' do
  #
  #   end
  # end
end
