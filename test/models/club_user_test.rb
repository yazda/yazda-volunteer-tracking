# == Schema Information
#
# Table name: club_users
#
#  id                    :integer          not null, primary key
#  club_id               :integer
#  user_id               :integer
#  receive_communication :boolean
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  role                  :integer          default("0")
#

require 'test_helper'

class ClubUserTest < ActiveSupport::TestCase
  should validate_presence_of(:club)
  should validate_uniqueness_of(:club_id).scoped_to(:user_id)

  should validate_presence_of(:user)
end
