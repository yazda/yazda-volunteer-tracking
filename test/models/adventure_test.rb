# == Schema Information
#
# Table name: adventures
#
#  id                      :integer          not null, primary key
#  yazda_adventure_id      :integer
#  name                    :string
#  start_time              :datetime
#  end_time                :datetime
#  owner_name              :string
#  owner_profile_image_url :string
#  track_time              :boolean          default("false")
#  club_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  address                 :string
#  adventure_type          :string
#  reportable_status       :integer          default("0")
#

require 'test_helper'

class AdventureTest < ActiveSupport::TestCase
  should(validate_numericality_of(:yazda_adventure_id)
           .only_integer
           .is_greater_than_or_equal_to(1))

  should validate_presence_of(:name)
  should validate_length_of(:name).is_at_most(255)

  should validate_presence_of(:start_time)
  should validate_presence_of(:end_time)

  should validate_presence_of(:owner_name)
  should validate_length_of(:owner_name).is_at_most(255)

  should validate_presence_of(:owner_profile_image_url)
  should validate_length_of(:owner_profile_image_url).is_at_most(255)

  should validate_presence_of(:club)

  should belong_to(:club)
  should have_many(:volunteers).dependent(:destroy)

  describe 'scopes' do
    describe '#in_date' do
      it 'retrieves all Adventures' do
        @adventures = Adventure.in_date(nil, nil)

        @adventures.count.must_equal 5
      end

      it 'filters by date' do
        start_time = '2017-01-09 20:26:21'
        end_time   = '2017-01-11 20:26:21'

        @adventures = Adventure.in_date(start_time, end_time)
        @adventures.count.must_equal 4

        @adventures.pluck(:name).wont_include 'Now'
      end

      it 'filters a single day' do
        time = DateTime.current.to_s

        @adventures = Adventure.in_date(time, time)
        @adventures.count.must_equal 1

        @adventures.pluck(:name).must_include 'Now'
      end
    end

    describe '#filter' do
      it 'filters Adventures' do
        adventure                   = Adventure.first
        adventure.something_context = 'hello,hi'
        adventure.save

        @adventures = Adventure.filter('hello')

        @adventures.count.must_equal 1
        @adventures.each do |a|
          a.something_context.must_include('hello')
        end
      end

      it 'returns all' do
        @adventures = Adventure.filter('')

        @adventures.count.must_equal 5
      end
    end

    describe '#search' do
      it 'searches for Adventures' do
        @adventures = Adventure.search('MyString')

        @adventures.count.must_equal 2
        @adventures.each do |a|
          a.name.must_equal 'MyString'
        end
      end

      it 'returns all' do
        @adventures = Adventure.search('')

        @adventures.count.must_equal 5
      end
    end
  end

  describe 'methods' do
    before do
      @adventure = Adventure.create(
        club:                    Club.first,
        start_time:              3.days.ago,
        end_time:                2.days.ago,
        name:                    'Testing a name',
        owner_name:              'Jeremey LastName',
        owner_profile_image_url: 'https://google.com/image'
      )
    end

    describe '#method_missing' do
      describe 'context_* = ' do
        it 'sets the value' do
          @adventure.something_context = 'hello,hi'
          @adventure.save!

          @adventure.tags_on("club_#{@adventure.club_id}_something_context")
            .map(&:name)
            .must_include 'hello'
          @adventure.tags_on("club_#{@adventure.club_id}_something_context")
            .map(&:name)
            .must_include 'hi'
        end

        it 'respond_to?' do
          @adventure.respond_to?(:something_context).must_equal true
        end
      end

      describe 'context_*' do
        it 'gets the value' do
          @adventure.something_context = 'hello,hi'
          @adventure.save!

          @adventure.something_context.must_include 'hello'
          @adventure.something_context.must_include 'hi'
        end

        it 'respond_to?' do
          @adventure.respond_to?(:something_context=).must_equal true
        end
      end
    end
  end
end
