# == Schema Information
#
# Table name: adventures
#
#  id                      :integer          not null, primary key
#  yazda_adventure_id      :integer
#  name                    :string
#  start_time              :datetime
#  end_time                :datetime
#  owner_name              :string
#  owner_profile_image_url :string
#  track_time              :boolean          default("false")
#  club_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  address                 :string
#  adventure_type          :string
#

require 'test_helper'

class EmailTest < ActiveSupport::TestCase
  should validate_presence_of(:subject)
  should validate_length_of(:subject).is_at_least(5).is_at_most(255)

  should validate_presence_of(:message)
  should validate_length_of(:message).is_at_least(5)

  should belong_to(:club)

  describe 'methods' do
    before do
      @email = Email.new(message: 'here is a test descriptikon',
                         subject: 'test')
    end

    describe '#message_markdown' do
      it 'should call #to_markdown' do
        Email.any_instance.expects(:to_markdown).with(@email.message)
        @email.message_markdown
      end
    end
  end
end
