# == Schema Information
#
# Table name: adventures
#
#  id                      :integer          not null, primary key
#  yazda_adventure_id      :integer
#  name                    :string
#  start_time              :datetime
#  end_time                :datetime
#  owner_name              :string
#  owner_profile_image_url :string
#  track_time              :boolean          default("false")
#  club_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  address                 :string
#  adventure_type          :string
#

require 'test_helper'

class IncentiveTest < ActiveSupport::TestCase
  should validate_presence_of(:hours)
  should(validate_numericality_of(:hours)
           .only_integer
           .is_greater_than(0))

  should validate_presence_of(:description)
  should validate_length_of(:description).is_at_least(10)

  should belong_to(:club)

  describe 'methods' do
    before do
      @incentive = Incentive.new(description: 'here is a test descriptikon',
                                 hours:       10)
    end

    describe '#description_markdown' do
      it 'should call #to_markdown' do
        Incentive.any_instance.expects(:to_markdown).with(@incentive.description)
        @incentive.description_markdown
      end
    end
  end
end
