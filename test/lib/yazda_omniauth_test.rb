require 'test_helper'.freeze

class YazdaOmniauthTest < ActiveSupport::TestCase
  def setup
    OmniAuth.config.test_mode = true
    @request                  = stub('Request')
    @request.stubs(:params).returns({})
    @request.stubs(:cookies).returns({})
    @request.stubs(:env).returns({})
    @request.stubs(:scheme).returns({})
    @request.stubs(:ssl?).returns(false)

    @client_id     = '123'
    @client_secret = '53cr3tz'
  end

  teardown do
    OmniAuth.config.test_mode = false
  end

  def strategy
    @strategy ||= begin
      args = [@client_id, @client_secret, @options].compact
      OmniAuth::Strategies::Yazda.new(nil, *args).tap do |strategy|
        strategy.stubs(:request).returns(@request)
      end
    end
  end

  test 'has correct Yazda site' do
    assert_equal 'https://api.yazdaapp.com', strategy.client.site
  end

  test 'has correct authorize url' do
    assert_equal '/oauth/authorize', strategy.client.options[:authorize_url]
  end

  test 'has correct token url with versioning' do
    @options = { client_options: { site: 'https://api.yazdaapp.com/v3' } }
    assert_equal '/oauth/token', strategy.client.options[:token_url]
    assert_equal 'https://api.yazdaapp.com/oauth/token',
                 strategy.client.token_url
  end

  test 'returns the default callback url (omitting querystring)' do
    url_base = 'http://auth.request.com'
    @request.stubs(:url).returns("#{url_base}/some/page")
    strategy.stubs(:script_name).returns('') # as not to depend on Rack env
    strategy.stubs(:query_string).returns('?foo=bar')
    assert_equal "#{url_base}/auth/yazda/callback", strategy.callback_url
  end

  test 'returns path from callback_path option (omitting querystring)' do
    @options = { callback_path: '/auth/FB/done' }
    url_base = 'http://auth.request.com'
    @request.stubs(:url).returns("#{url_base}/page/path")
    strategy.stubs(:script_name).returns('') # as not to depend on Rack env
    strategy.stubs(:query_string).returns('?foo=bar')
    assert_equal "#{url_base}/auth/FB/done", strategy.callback_url
  end

  test 'returns the id from raw_info' do
    strategy.stubs(:raw_info).returns('user' => { 'id' => '123' })
    assert_equal '123', strategy.uid
  end
end
