require 'test_helper'

class SyncClubAdventuresWorkerTest < ActiveSupport::TestCase
  it 'should call adventure sync service' do
    club = Club.first

    Club.expects(:find).with(1).returns(club)
    AdventureSyncService.any_instance.expects(:sync_club_adventures)

    worker = SyncClubAdventuresWorker.new
    worker.perform(1)
  end
end
