require 'sidekiq/web'
require 'sidekiq-scheduler'
require 'sidekiq-scheduler/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  get 'charts/volunteer'
  get 'charts/tags'
  get 'charts/adventures'

  root to: 'home#index'

  get '/auth/:provider/callback', to: 'sessions#create'
  get '/auth/failure', to: 'sessions#failure'

  get 'login' => 'home#index', as: :login
  delete 'logout', to: 'sessions#destroy'

  patch '/select_club/:id', to: 'home#select_club', as: :select_club

  resource :account, only: [:show, :update]

  resources :tags, only: :index
  resources :emails, only: :create
  resources :users, only: [:create, :update, :show]
  resources :volunteers, except: [:edit, :destroy, :show]

  resources :reports do
    resources :report_items, only: [:create, :update, :destroy], shallow: true
  end

  resources :clubs, only: [:index, :edit, :update] do
    member do
      patch :enable
      patch :disable
    end
  end

  resources :adventures, except: [:destroy, :show] do
    member do
      post :track_time
      post :set_time_for_volunteers
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
