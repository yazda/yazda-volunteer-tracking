require Rails.root.join('lib', 'yazda_omniauth')

Rails.application.config.middleware.use OmniAuth::Builder do
  if ENV['YAZDA_ENDPOINT']
    provider :yazda, ENV['YAZDA_API_KEY'], ENV['YAZDA_SECRET'],
             client_options: { site:          ENV['YAZDA_ENDPOINT'],
                               authorize_url: "/oauth/authorize",
                               token_url:     '/oauth/token' }
  else
    provider :yazda, ENV['YAZDA_API_KEY'], ENV['YAZDA_SECRET']
  end
end
